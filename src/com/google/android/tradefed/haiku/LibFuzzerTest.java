/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.android.tradefed.haiku;

import com.android.tradefed.device.CollectingOutputReceiver;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.result.ITestInvocationListener;
import com.android.tradefed.testtype.IRemoteTest;
import com.android.tradefed.testtype.IDeviceTest;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.config.Option;
import com.android.tradefed.util.FileUtil;

import com.google.common.base.Strings;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class LibFuzzerTest implements IRemoteTest, IDeviceTest {

    private ITestDevice mDevice;

    @Option(
        name = "fuzzer_path",
        shortName = 'f',
        description = "Local path to the fuzzer",
        importance = Option.Importance.ALWAYS
    )
    private String mFuzzerPath = "fuzzer";

    @Option(
        name = "corpus_path",
        shortName = 'c',
        description = "Local path to the corpus",
        importance = Option.Importance.ALWAYS
    )
    private String mCorpusPath = "corpus";

    @Option(
        name = "session_id",
        shortName = 's',
        description = "Identifier for the fuzzing session",
        importance = Option.Importance.ALWAYS
    )
    private String mSessionId = "fuzz";

    @Option(
        name = "fuzzer_args",
        shortName = 'r',
        description = "Additional arguments to pass to fuzzer",
        importance = Option.Importance.ALWAYS
    )
    private String mFuzzerArgs = "";

    @Option(
        name = "coverage_data",
        shortName = 'd',
        description = "Gather coverage data",
        importance = Option.Importance.ALWAYS
    )
    private boolean mCoverageData = false;

    @Option(
        name = "timeout",
        shortName = 't',
        description = "Timeout for fuzzer",
        importance = Option.Importance.ALWAYS
    )
    private int mTimeout = 10 * 60;

    @Override
    public void setDevice(ITestDevice device) {
        mDevice = device;
    }

    @Override
    public ITestDevice getDevice() {
        return mDevice;
    }

    private void pullPattern(String path, String pattern) throws DeviceNotAvailableException {
        String lssancov = getDevice().executeShellCommand("ls -1 " + path + pattern);
        String items[] = lssancov.split("\r?\n");
        for (String item : items) {
            if (item.isEmpty()) {
                continue;
            }
            String deviceFile = item;
            File localFile =
                    new File(
                            String.format("./%s-%s/", mFuzzerPath, mSessionId),
                            item.substring(item.lastIndexOf("/")));

            if (!getDevice().pullFile(deviceFile, localFile)) {
                CLog.i("Could not retrieve %s", item);
            }
        }
    }

    @Override
    public void run(ITestInvocationListener listener) throws DeviceNotAvailableException {
        // Make sure we're running as root
        if (!getDevice().isAdbRoot()) {
            if (!getDevice().enableAdbRoot()) {
                return;
            }
        }
        String fuzzerpath = String.format("/data/nativetest/fuzzers/%s", mFuzzerPath);

        // Use the provided params to determine if the fuzzer is on target
        String corpuspath = String.format("%s/%s", fuzzerpath, mCorpusPath);
        int corpus_entries = 0;
        CLog.i("setup corpus for fuzzing");
        if (!getDevice().doesFileExist(corpuspath)) {
            CLog.i("Corpus directory not found on device, creating");
            getDevice().executeShellCommand("mkdir -p " + corpuspath);
            File local_dir =
                    new File(
                            String.format("./%s-%s/", mFuzzerPath, mSessionId),
                            String.format("%s-%s", mFuzzerPath, mCorpusPath));
            if (!local_dir.exists()) {
                if (!local_dir.mkdirs()) {
                    CLog.w("Failed to create corpus directory %s", local_dir.getAbsolutePath());
                }
            }
            getDevice().pushDir(local_dir, corpuspath);
            if (!getDevice().doesFileExist(corpuspath)) {
                CLog.i("Corpus could not be pushed to device");
                return;
            }
            corpus_entries =
                    Integer.parseInt(
                            getDevice()
                                    .executeShellCommand("ls -1 " + corpuspath + " | wc -l")
                                    .replace("\n", ""));
        }

        CLog.i("Corpus setup done. Found %d entries. Build fuzzing command", corpus_entries);

        // build the fuzzer cmd line
        String fuzzer_cmdline = String.format("cd /data && %s/%s", fuzzerpath, mFuzzerPath);

        // add fuzzer args
        if (!Strings.isNullOrEmpty(mFuzzerArgs)) fuzzer_cmdline += " " + mFuzzerArgs;

        // add coverage flags if required
        if (mCoverageData) {
            fuzzer_cmdline += " -dump_coverage=1";
        }

        // set the output file names
        fuzzer_cmdline += " -artifact_prefix=" + fuzzerpath + "-" + mSessionId;

        // add corpus dir
        fuzzer_cmdline += " " + corpuspath;

        //set the timeout to something reasonable for libFuzzer
        fuzzer_cmdline = String.format("%s -max_total_time=%d", fuzzer_cmdline, mTimeout);

        //run the fuzzer with timeout
        CollectingOutputReceiver receiver = new CollectingOutputReceiver();
        getDevice()
                .executeShellCommand(fuzzer_cmdline, receiver, mTimeout + 60, TimeUnit.SECONDS, 1);
        String fuzz_output = receiver.getOutput();

        CLog.i("libFuzzer Output is %s\n", fuzz_output);
        //parse results
        String[] fuzz_tokens = fuzz_output.split("\n");

        String crash_name = "";
        String search_str = "Test unit written to ";
        for (int i = fuzz_tokens.length - 1; i >= 0; i--) {
            if (fuzz_tokens[i].contains(search_str)) {
                crash_name =
                        fuzz_tokens[i].substring(
                                fuzz_tokens[i].indexOf(search_str) + search_str.length());
                break;
            }
        }

        //get any crashes
        if (!Strings.isNullOrEmpty(crash_name)) {
            CLog.i(
                    "Creating local file %s and associated stack trace",
                    crash_name.substring(crash_name.lastIndexOf("/")));
            File crash_file =
                    new File(
                            String.format("./%s-%s/", mFuzzerPath, mSessionId),
                            crash_name.substring(crash_name.lastIndexOf("/")));

            getDevice().pullFile(crash_name, crash_file);

            CLog.i(String.format("Stack trace file: ./%s-%s-trace/", mFuzzerPath, mSessionId));
            File trace_file =
                    new File(
                            String.format("./%s-%s/", mFuzzerPath, mSessionId),
                            String.format(
                                    "%s-trace", crash_name.substring(crash_name.lastIndexOf("/"))));
            try {
                FileUtil.writeToFile(fuzz_output, trace_file);
            } catch (IOException e) {
                CLog.i(
                        String.format(
                                "Failure writing to : ./%s-%s-trace/: %s",
                                mFuzzerPath, mSessionId, e.toString()));
            }
        } else {
            CLog.i("no crash found");
        }
        //get coverage data
        if (mCoverageData) {
            pullPattern("/data/", "*.sancov");
            pullPattern("/data/", "*.sancov.map");
        }

        //get new corpus
        int new_corpus_entries =
                Integer.parseInt(
                        getDevice()
                                .executeShellCommand("ls -1 " + corpuspath + " | wc -l")
                                .replace("\n", ""));
        CLog.i("After fuzzing, there are %d corpus entries", new_corpus_entries);
        if (corpus_entries < new_corpus_entries) {
            File local_dir =
                    new File(
                            String.format("./%s-%s/", mFuzzerPath, mSessionId),
                            String.format("%s-%s", mFuzzerPath, mCorpusPath));
            if (!local_dir.exists()) {
                if (!local_dir.mkdirs()) {
                    CLog.w("Failed to create corpus directory %s", local_dir.getAbsolutePath());
                }
            }

            getDevice().pullDir(corpuspath, local_dir);
        }
    }
}
