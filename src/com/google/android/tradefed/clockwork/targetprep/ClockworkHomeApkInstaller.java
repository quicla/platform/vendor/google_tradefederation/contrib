// Copyright 2017 Google Inc. All Rights Reserved.
package com.google.android.tradefed.clockwork.targetprep;

import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.targetprep.BuildError;
import com.android.tradefed.targetprep.ITargetPreparer;
import com.android.tradefed.targetprep.InstallApkSetup;
import com.android.tradefed.targetprep.TargetSetupError;
import com.google.android.tradefed.util.RollupClFetcher;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A {@link ITargetPreparer} that installs one clockwork home apk located on x20 folder.
 *
 * <p>All release candidates of home apk will be stored under x20 folder:
 * /teams/clockwork/apks/home/. This target preparer will find the right apk to install on device
 * based on options provided. The apk parent folder path follows
 * "clockwork.home_<date>_<time>_RC<RC_Number>" pattern.
 *
 * <p>Home apk Roll-up CL number will be posted onto invocation context.
 */
@OptionClass(alias = "install-cw-home-apk")
public class ClockworkHomeApkInstaller extends InstallApkSetup {

    private static final String CLOCKWORK_HOME_PROJECT_NAME = "clockwork.home";
    private static final String HOME_APK_ROLLUP_CL_KEY = "home_apk_rollup_cl";
    private static final String UNKNOWN_CL_NUMBER = "0";
    private static final Pattern CLOCKWORK_HOME_CANDIDATE_NAME_PATTERN =
            Pattern.compile(String.format("^%s.*?_RC[0-9][0-9]$", CLOCKWORK_HOME_PROJECT_NAME));

    @Option(name = "parent-folder-name", description = "Parent folder path release candidates.")
    private String mParentFolderName = "/google/data/ro/teams/clockwork/apks/home/armeabi-v7a/";

    @Option(name = "sub-folder-name", description = "Subfolder name to look for apk.")
    private String mSubFolderName = "latest";

    @Option(name = "apk-name", description = "Name of apk to be installed")
    private String mApkName = "ClockworkHomeGoogleRelease.apk";

    @Option(name = "post-install-reboot", description = "Reboot after installation.")
    private Boolean mPostInstallReboot = true;

    @Option(name = "post-install-reboot-timeout", description = "Reboot timeout in msec.")
    private long mPostInstallRebootTimeout = 120 * 1000;

    @Option(name = "disable-rollup-cl", description = "Disable fetching roll-up CL number.")
    private Boolean mDisableRollupCl = true;

    @Override
    public void setUp(ITestDevice device, IBuildInfo buildInfo)
            throws TargetSetupError, BuildError, DeviceNotAvailableException {
        if (isDisabled()) {
            CLog.d("This target preparer has been disabled.");
            return;
        }
        Path parentFolderPath = Paths.get(mParentFolderName);
        Path subFolderPath = Paths.get(mParentFolderName, mSubFolderName);
        if (!Files.exists(subFolderPath)) {
            throw new TargetSetupError(
                    String.format(
                            "Cannot find path: %s. Please double check. "
                                    + "Access to clockwork companion apk requires "
                                    + "clockwork permission. If you run the test locally, "
                                    + "a work around is to disable this target preparer by "
                                    + "--install-cw-home-apk:disable. And install a local apk "
                                    + "through InstallApkSetup target preparer.",
                            subFolderPath.toString()),
                    device.getDeviceDescriptor());
        }
        Path homeApkPath = Paths.get(mParentFolderName, mSubFolderName, mApkName);
        CLog.d("About to install clockwork home apk: %s", homeApkPath.toString());
        getApkPaths().add(homeApkPath.toFile());
        if (!mDisableRollupCl) {
            buildInfo.addBuildAttribute(
                    HOME_APK_ROLLUP_CL_KEY, findRollupClNumber(parentFolderPath, subFolderPath));
        }
        super.setUp(device, buildInfo);
        if (mPostInstallReboot) {
            CLog.d("Rebooting device.");
            device.rebootUntilOnline();
            device.waitForBootComplete(mPostInstallRebootTimeout);
        }
    }

    private String findRollupClNumber(Path parentPath, Path subFolderPath) {
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(parentPath)) {
            Path destPath = Files.readSymbolicLink(subFolderPath);
            for (Path rcFilePath : stream) {
                if (Files.isSymbolicLink(rcFilePath)
                        && Files.readSymbolicLink(rcFilePath)
                                .toString()
                                .equals(destPath.toString())) {
                    String candidateName = rcFilePath.getFileName().toString();
                    Matcher m = CLOCKWORK_HOME_CANDIDATE_NAME_PATTERN.matcher(candidateName);
                    if (!m.matches()) {
                        CLog.e("Candidate name %s doesn't have correct format.", candidateName);
                        return UNKNOWN_CL_NUMBER;
                    }
                    // Remove last five characters "_RC00" to get release name
                    String releaseName = candidateName.substring(0, candidateName.length() - 5);
                    CLog.d(
                            "Calling util method to get cl number for releaseName: %s,"
                                    + "candidateName: %s",
                            releaseName, candidateName);
                    return new RollupClFetcher()
                            .fetch(CLOCKWORK_HOME_PROJECT_NAME, releaseName, candidateName);
                }
            }
        } catch (IOException ioe) {
            CLog.e(ioe);
        } catch (RollupClFetcher.RollupClException re) {
            CLog.e(re);
        }
        return UNKNOWN_CL_NUMBER;
    }
}
