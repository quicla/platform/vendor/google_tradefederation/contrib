// Copyright 2017 Google Inc. All Rights Reserved.

package com.google.android.tradefed.clockwork.ios;

import com.android.ddmlib.testrunner.TestIdentifier;
import com.google.android.tradefed.clockwork.ClockworkConnectivityTest;
import com.google.android.tradefed.clockwork.ConnectivityHelper;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.result.ITestInvocationListener;
import com.android.tradefed.util.RunUtil;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;

/**
 * A {@link ConnectivityTestRunner} that test android wear for iOS bluetooth longevity with ncd, The
 * test will check battery and connectivity data during the test.
 */
@OptionClass(alias = "cw-ios-bt-longevity")
public class ClockworkIosBtLongevity extends ClockworkConnectivityTest {

    @Option(
        name = "test-period",
        description = "Time period for USB cable disconnection during test, default to 3600s"
    )
    private long mTestPeriod = 60 * 60;

    @Option(name = "usb-switch-type", description = "The the type of USB switching, default is ncd")
    private String mUsbSwitchType = "ncd";

    @Option(
        name = "usb-switch-port-id",
        description = "The port number on ncd connection, default -1",
        mandatory = true
    )
    private int mNcdId = -1;

    @Option(name = "test-run-name", description = "Test run name for different reports")
    protected String mTestRunName = "ClockworkIosBtLongevity";

    private static final String CONNECT = "CONNECT";
    private static final String DISCONNECT = "DISCONNECT";
    private static final int DUMP_SYS_TIMEOUT = 60;
    private ConnectivityHelper mHelper;

    /**
     * This test will run downloader command multiple times to get average Calculate results and
     * report
     */
    @Override
    public void run(ITestInvocationListener listener) throws DeviceNotAvailableException {

        long start = System.currentTimeMillis();
        mHelper = new ConnectivityHelper();
        int success = 1;
        int batteryLevelBegin = -1;
        int batteryLevelEnd = -1;
        long current = 0;
        listener.testRunStarted(mTestRunName, 0);
        TestIdentifier id = new TestIdentifier(getClass().getCanonicalName(), "iOS Longevity test");
        listener.testStarted(id);
        // Get initial battery level
        batteryLevelBegin = mHelper.getBatteryLevel(getDevice());
        CLog.i("Confirm bluetooth connection is up before the test start");
        if (!mHelper.validateConnectionState(getDevice(), DUMP_SYS_TIMEOUT)) {
            mHelper.captureLogs(listener, 0, "Watch", getDevice());
            listener.testFailed(id, "BLE connection is not up at test start");
            success = 0;
        } else {
            CLog.i("Disconnect NCD");
            Assert.assertTrue(mHelper.ncdAction(DISCONNECT, mNcdId));
            // Just wait until time pass
            CLog.i("Wait %d seconds", mTestPeriod);
            RunUtil.getDefault().sleep(mTestPeriod * 1000);
            CLog.i("Reconnect NCD");
            Assert.assertTrue(mHelper.ncdAction(CONNECT, mNcdId));
            getDevice().waitForDeviceAvailable();
            // Get battery level after reconnect
            batteryLevelEnd = mHelper.getBatteryLevel(getDevice());
            RunUtil.getDefault().sleep(30 * 1000);
            if (!mHelper.validateConnectionState(getDevice(), DUMP_SYS_TIMEOUT)) {
                mHelper.captureLogs(listener, 1, "Watch", getDevice());
                listener.testFailed(id, "BLE connection is not up at test end");
                success = 0;
            }
        }
        listener.testEnded(id, Collections.emptyMap());
        // Report results
        Map<String, String> metrics = new HashMap<String, String>();
        CLog.i("Success %d Battery begin %d, end %d", success, batteryLevelBegin, batteryLevelEnd);
        metrics.put("Success", String.format("%d", success));
        metrics.put("Beginning Battery Level", String.format("%d", batteryLevelBegin));
        metrics.put("Ending Battery Level", String.format("%d", batteryLevelEnd));
        metrics.put(
                "reconnect_count",
                Integer.toString(mHelper.wearableTransportReconnectCount(getDevice())));
        metrics.put(
                "bluetooth_disconnect",
                Integer.toString(mHelper.bluetoothDisconnectCount(getDevice())));
        metrics.put(
                "bluetooth_manager_disconnect",
                Integer.toString(mHelper.bluetoothManagerDisconnectEventsCount(getDevice())));
        listener.testRunEnded(System.currentTimeMillis() - start, metrics);
    }
}
