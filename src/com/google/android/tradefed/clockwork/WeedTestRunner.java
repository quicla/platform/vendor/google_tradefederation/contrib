// Copyright 2017 Google Inc. All Rights Reserved.
package com.google.android.tradefed.clockwork;

import com.android.annotations.VisibleForTesting;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.Option.Importance;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.invoker.IInvocationContext;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.result.ByteArrayInputStreamSource;
import com.android.tradefed.result.FileInputStreamSource;
import com.android.tradefed.result.ITestInvocationListener;
import com.android.tradefed.result.InputStreamSource;
import com.android.tradefed.result.LogDataType;
import com.android.tradefed.testtype.IInvocationContextReceiver;
import com.android.tradefed.util.CommandResult;
import com.android.tradefed.util.CommandStatus;
import com.android.tradefed.util.FileUtil;
import com.android.tradefed.util.IRunUtil;
import com.android.tradefed.util.RunUtil;
import com.android.tradefed.util.SubprocessTestResultsParser;
import com.google.android.tradefed.testtype.ClockworkTest;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * WEED (Wear End-to-End test Driver) test driver in TradeFed.
 *
 * <p>WEED is an automated end-to-end test driver, which can send adb commands to multiple devices
 * on the linux host. It is developed and maintained by the Android wear team. For more information,
 * please refer to go/cw-weed.
 *
 * <p>WeedTestRunner is the integration of WEED into TradeFed so WEED tests can be in the lab.
 */
public class WeedTestRunner extends ClockworkTest implements IInvocationContextReceiver {

    private static final Pattern TEST_NAME_PATTERN =
            Pattern.compile(".*/(\\w+)_binary_deploy\\.jar");
    private static final String FILE_NAME_PATTERN_STRING = "(\\w+)_binary_deploy\\.jar";
    private static final String UNDECLARAED_FOLDER_ENV = "TEST_UNDECLARED_OUTPUTS_DIR";

    @Option(name = "adb-path", description = "adb instance path for interacting with device.")
    private String mAdbPath = "adb";

    @Option(
        name = "extra-args",
        description = "Any extra arguments that need to pass to WEED binary. E.g. key=value"
    )
    private Map<String, String> mExtraArgs = new HashMap<>();

    @Option(
        name = "test-jar-path",
        description = "Path to test jar or folder. Tests in folder will be run in series.",
        mandatory = true,
        importance = Importance.ALWAYS
    )
    private String mTestJarPath;

    @Option(name = "test-run-name", description = "Name to identify test run")
    private String mTestRunName = "WeedTest";

    @Option(
        name = "test-timeout",
        description = "A timeout in MS for single WEED test jar to run.",
        isTimeVal = true
    )
    private long mTestTimeoutMs = 60 * 60 * 1000; // 60m

    private IInvocationContext invocationContext;

    @Override
    public void setInvocationContext(IInvocationContext iInvocationContext) {
        this.invocationContext = iInvocationContext;
    }

    @Override
    public void run(ITestInvocationListener listener) throws DeviceNotAvailableException {
        try {
            Path testFolderPath = Paths.get(mTestJarPath);
            File testFolderFile = null;
            if (Files.isSymbolicLink(testFolderPath)) {
                testFolderFile = testFolderPath.toRealPath().toFile();
            } else {
                testFolderFile = testFolderPath.toFile();
            }
            Set<String> testJarSet = FileUtil.findFiles(testFolderFile, FILE_NAME_PATTERN_STRING);
            String runName;
            for (String testJar : testJarSet) {
                CLog.d("Found test jar: %s", testJar);
                File tempFolder = null;
                try {
                    Matcher testNameMatcher = TEST_NAME_PATTERN.matcher(testJar);
                    if (testNameMatcher.matches()) {
                        runName = testNameMatcher.group(1);
                    } else {
                        runName = mTestRunName;
                    }
                    tempFolder = FileUtil.createTempDir(runName);
                    ByteArrayOutputStream stderrOutputStream = new ByteArrayOutputStream();
                    ByteArrayOutputStream stdoutOutputStream = new ByteArrayOutputStream();
                    IRunUtil runUtil = new RunUtil();
                    runUtil.setEnvVariable(UNDECLARAED_FOLDER_ENV, tempFolder.getAbsolutePath());
                    CommandResult cr =
                            runUtil.runTimedCmd(
                                    mTestTimeoutMs,
                                    stdoutOutputStream,
                                    stderrOutputStream,
                                    constructCommand(testJar).toArray(new String[0]));
                    CommandStatus cs = cr.getStatus();
                    InputStreamSource stdoutStream =
                            new ByteArrayInputStreamSource(stdoutOutputStream.toByteArray());
                    listener.testLog(
                            String.format("%s_stdout", runName), LogDataType.TEXT, stdoutStream);
                    InputStreamSource stderrStream =
                            new ByteArrayInputStreamSource(stderrOutputStream.toByteArray());
                    listener.testLog(
                            String.format("%s_stderr", runName), LogDataType.TEXT, stderrStream);
                    try (DirectoryStream<Path> stream =
                            Files.newDirectoryStream(Paths.get(tempFolder.getAbsolutePath()))) {
                        for (Path filesPath : stream) {
                            InputStreamSource unDeclaredOutputFileStream =
                                    new FileInputStreamSource(filesPath.toFile());
                            // Figure out different log data type based on extension.
                            String fileFullName = filesPath.getFileName().toString();
                            String fileExtension = FileUtil.getExtension(fileFullName);
                            String fileBaseName = FileUtil.getBaseName(fileFullName);
                            LogDataType logDataType = LogDataType.UNKNOWN;
                            for (LogDataType candidateLogDataType : LogDataType.values()) {
                                if (fileExtension.equals("." + candidateLogDataType.getFileExt())) {
                                    logDataType = candidateLogDataType;
                                    break;
                                }
                            }
                            listener.testLog(
                                    String.format("%s_%s", runName, fileBaseName),
                                    logDataType,
                                    unDeclaredOutputFileStream);
                            unDeclaredOutputFileStream.cancel();
                        }
                    } catch (IOException e) {
                        CLog.e("Cannot read unDeclaredOutputFolder. %s", e);
                    }
                    if (cs.equals(CommandStatus.TIMED_OUT)) {
                        String message =
                                String.format(
                                        "Test timed out with status: %s", cs.toString());
                        CLog.e(message);
                        listener.testRunStarted(runName, 0);
                        listener.testRunFailed(message);
                        listener.testRunEnded(0, Collections.emptyMap());
                        continue;
                    }
                    SubprocessTestResultsParser parser =
                            new SubprocessTestResultsParser(listener, invocationContext);
                    File testResultFile = new File(tempFolder, "testResults.txt");
                    if (testResultFile.exists()) {
                        parser.parseFile(testResultFile);
                    } else {
                        // Test result file is missing. We need manually invoke listener callback
                        // functions to indicate test run failed.
                        CLog.e("Test result is missing for test run: %s", runName);
                        listener.testRunStarted(runName, 0);
                        listener.testRunFailed("No testResults.txt");
                        listener.testRunEnded(0, Collections.emptyMap());
                    }
                } catch (IOException e) {
                    CLog.d("Cannot create temp file. Error: %s", e.toString());
                } finally {
                    FileUtil.recursiveDelete(tempFolder);
                }
            }
        } catch (IOException e) {
            CLog.e("IOException when to find test jar folder. %s", e);
        }
    }

    @VisibleForTesting()
    List<String> constructCommand(String testJar) {
        List<String> commandBuilder = new ArrayList<>();
        commandBuilder.add("java");
        commandBuilder.add("-jar");
        commandBuilder.add(testJar);
        commandBuilder.add("--watchId");
        commandBuilder.add(getDevice().getSerialNumber());
        commandBuilder.add("--androidPhoneId");
        commandBuilder.add(getCompanion().getSerialNumber());
        commandBuilder.add("--adbPath");
        commandBuilder.add(mAdbPath);
        for (String s : mExtraArgs.keySet()) {
            commandBuilder.add(String.format("--%s", s));
            if (mExtraArgs.get(s) != null) {
                commandBuilder.add(mExtraArgs.get(s));
            }
        }
        CLog.d("Final command to run is %s", commandBuilder.toString());
        return commandBuilder;
    }
}
