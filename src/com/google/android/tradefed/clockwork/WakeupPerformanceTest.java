package com.google.android.tradefed.clockwork;

import com.android.tradefed.config.Option;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.device.LogcatReceiver;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.result.ITestInvocationListener;
import com.android.tradefed.result.InputStreamSource;
import com.android.tradefed.testtype.IDeviceTest;
import com.android.tradefed.testtype.IRemoteTest;
import com.android.tradefed.util.RunUtil;
import com.android.tradefed.util.SimpleStats;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/** A test to measure the time it takes to wake up from ambient mode. The test will put the device
 * to sleep and wake it up several times, reporting statistics for the amount of time taken. The
 * times will be extracted from the logcat data. */
public final class WakeupPerformanceTest implements IDeviceTest, IRemoteTest {

    private static final String LOGCAT_CMD = "logcat -v monotonic";
    private static final Pattern INPUT_ENTRY = Pattern.compile("^\\s*(\\d*\\.\\d*)\\s*\\d*\\s*\\d*"
            + "\\s*I Input\\s*: injectKeyEvent: KeyEvent \\{ "
            + "action=ACTION_DOWN, keyCode=KEYCODE_WAKEUP.*");
    private static final Pattern DISPLAY_ENTRY = Pattern.compile("^\\s*(\\d*\\.\\d*)\\s*\\d*\\s*"
            + "\\d*\\s*D qdhwcomposer: hwc_setPowerMode: Done setting mode 2 on display 0$");
    private static final int LOGCAT_SIZE = 20971520; //20 mb

    private ITestDevice mDevice;
    private LogcatReceiver mLogcat;

    @Option(name = "iterations", description = "Number of times to wakeup the screen")
    private int mIterations = 10;

    @Option(name = "test-key", description = "Test key to use when posting to the dashboard")
    private String mTestKey = "WakeupPerformanceTest";

    @Override
    public void run(ITestInvocationListener listener) throws DeviceNotAvailableException {
        ArrayList<String> times = new ArrayList<String>();
        SimpleStats stats = new SimpleStats();
        mLogcat = new LogcatReceiver(getDevice(), LOGCAT_CMD, LOGCAT_SIZE, 0);
        mLogcat.start();
        listener.testRunStarted(mTestKey, 0);
        for (int i = 0; i < mIterations; i++) {
            mDevice.executeShellCommand("input keyevent 223");
            RunUtil.getDefault().sleep(1000);
            mLogcat.clear();
            mDevice.executeShellCommand("input keyevent 224");
            RunUtil.getDefault().sleep(1000);
            double time = parseLogcat();
            if (time != -1) {
                time = time * 1000;
                stats.add(time);
                times.add(Double.toString(time));
            }

        }
        CLog.i("List of wakeup times in ms: %s", String.join(",", times));
        listener.testRunEnded(0, getMetrics(stats));
        mLogcat.stop();

    }

    private Map<String, String> getMetrics(SimpleStats stats) {
        Map<String, String> metrics = new HashMap<String, String>();
        CLog.i(mTestKey + " Min: " + stats.min().toString());
        CLog.i(mTestKey + " Max: " + stats.max().toString());
        CLog.i(mTestKey + " Avg: " + stats.mean().toString());
        CLog.i(mTestKey + " Median: " + stats.median().toString());
        CLog.i(mTestKey + " Std Dev: " + stats.stdev().toString());

        metrics.put(mTestKey + "_min",  stats.min().toString());
        metrics.put(mTestKey + "_max",  stats.max().toString());
        metrics.put(mTestKey + "_avg",  stats.mean().toString());
        metrics.put(mTestKey + "_median",  stats.median().toString());
        metrics.put(mTestKey + "_std_dev",  stats.stdev().toString());

        return metrics;
    }

    private static Matcher matches(Pattern pattern, String line) {
        Matcher ret = pattern.matcher(line);
        return ret.matches() ? ret : null;
    }

    private double parseLogcat() {
        boolean found_input = false;
        boolean found_display = false;
        String line;
        Matcher match = null;
        double time = -1;
        try (InputStreamSource input = mLogcat.getLogcatData();
                BufferedReader br =
                new BufferedReader(new InputStreamReader(input.createInputStream()))) {
            while ((line = br.readLine()) != null) {
                if (!found_input && ((match = matches(INPUT_ENTRY, line)) != null)) {
                    found_input = true;
                    time = Double.parseDouble(match.group(1));
                }
                if (found_input && ((match = matches(DISPLAY_ENTRY, line)) != null)) {
                    found_display = true;
                    time = Double.parseDouble(match.group(1)) - time;
                }
            }
        } catch (IOException io) {
            CLog.e(io);
        } finally {
            if (!(found_input && found_display)) {
              return -1;
            }
            return time;
        }
    }

    @Override
    public void setDevice(ITestDevice device) {
        mDevice = device;
    }

    @Override
    public ITestDevice getDevice() {
        return mDevice;
    }
}
