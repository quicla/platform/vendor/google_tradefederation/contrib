// Copyright 2017 Google Inc. All Rights Reserved.

package com.google.android.tradefed.adb;

import com.android.tradefed.result.ByteArrayInputStreamSource;
import com.android.tradefed.result.FileInputStreamSource;
import com.android.tradefed.result.ITestInvocationListener;
import com.android.tradefed.result.InputStreamSource;
import com.android.tradefed.result.LogDataType;
import com.android.tradefed.util.CommandResult;
import com.android.tradefed.util.CommandStatus;
import com.android.tradefed.util.IRunUtil;

import java.io.File;

/** Utility to retrieve and report adb diagnostics info. */
public class AdbLogUtil {

    private IRunUtil mRunUtil;
    private static final long CMD_TIMEOUT = 60 * 1000L;

    /** Create an {@link AdbLogUtil} with the given {@link IRunUtil} to collect logs. */
    public AdbLogUtil(IRunUtil runUtil) {
        mRunUtil = runUtil;
    }

    /** Enable logging on the host and device. May affect adb performance. */
    public void enableLogging() {
        // adb log from host
        // do not use "all" - it collects too much information and significantly slows adb down
        mRunUtil.setEnvVariable("ADB_TRACE", "sync sysdeps jdwp services auth shell");
        mRunUtil.setEnvVariable("ANDROID_LOG_TAGS", "*:v");

        // adbd log from device
        mRunUtil.runTimedCmd(CMD_TIMEOUT, "adb shell setprop persist.adb.trace_mask 0".split(" "));
        mRunUtil.runTimedCmd(CMD_TIMEOUT, "adb shell rm /data/adb/*".split(" "));
    }

    private InputStreamSource getHostAdbLog() {
        CommandResult cr = mRunUtil.runTimedCmd(CMD_TIMEOUT, "id -u".split(" "));
        String uid = cr.getStdout().split("\n")[0];
        File adbLog = new File(String.format("/tmp/adb.%s.log", uid));
        if (adbLog.exists()) {
            return new FileInputStreamSource(adbLog);
        } else {
            return new ByteArrayInputStreamSource("Failed to get host adb log.".getBytes());
        }
    }

    private InputStreamSource getHostDmesgLog() {
        CommandResult cr = mRunUtil.runTimedCmd(CMD_TIMEOUT, "dmesg -T -s 4096".split(" "));
        if (cr.getStatus() == CommandStatus.SUCCESS) {
            return new ByteArrayInputStreamSource(cr.getStdout().getBytes());
        } else {
            String failMessage =
                    String.format(
                            "Failed to get host dmesg log.\n%s\n%s",
                            cr.getStdout(), cr.getStderr());
            return new ByteArrayInputStreamSource(failMessage.getBytes());
        }
    }

    private InputStreamSource getDeviceAdbdLog() {
        CommandResult cr = mRunUtil.runTimedCmd(CMD_TIMEOUT, "adb shell cat /data/adb/*".split(" "));
        if (cr.getStatus() == CommandStatus.SUCCESS) {
            return new ByteArrayInputStreamSource(cr.getStdout().getBytes());
        } else {
            String failMessage =
                    String.format(
                            "Failed to get device adbd log.\n%s\n%s",
                            cr.getStdout(), cr.getStderr());
            return new ByteArrayInputStreamSource(failMessage.getBytes());
        }
    }

    private InputStreamSource getDeviceDmesgLog() {
        CommandResult cr = mRunUtil.runTimedCmd(CMD_TIMEOUT, "adb shell dmesg -T -s 4096".split(" "));
        if (cr.getStatus() == CommandStatus.SUCCESS) {
            return new ByteArrayInputStreamSource(cr.getStdout().getBytes());
        } else {
            String failMessage =
                    String.format(
                            "Failed to get device dmesg log.\n%s\n%s",
                            cr.getStdout(), cr.getStderr());
            return new ByteArrayInputStreamSource(failMessage.getBytes());
        }
    }

    /** Report all collected logs to the given {@link ITestInvocationListener}. */
    public void reportAllLogs(ITestInvocationListener listener) {
        // we expect some logs are unavailable when adb fails. The "getXLog" functions will always
        // return an InputStreamSource, containing either the log or an error message
        listener.testLog("host_log_adb", LogDataType.TEXT, getHostAdbLog());
        listener.testLog("host_log_dmesg", LogDataType.TEXT, getHostDmesgLog());
        listener.testLog("device_log_adbd", LogDataType.TEXT, getDeviceAdbdLog());
        listener.testLog("device_log_dmesg", LogDataType.TEXT, getDeviceDmesgLog());
    }
}
