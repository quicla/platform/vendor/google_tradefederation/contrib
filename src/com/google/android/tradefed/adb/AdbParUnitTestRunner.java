/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.android.tradefed.adb;

import com.android.ddmlib.MultiLineReceiver;
import com.android.ddmlib.testrunner.ITestRunListener;
import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.build.VersionedFile;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.result.ITestInvocationListener;
import com.android.tradefed.testtype.IBuildReceiver;
import com.android.tradefed.testtype.IRemoteTest;
import com.android.tradefed.testtype.PythonUnitTestResultParser;
import com.android.tradefed.util.ArrayUtil;
import com.android.tradefed.util.CommandResult;
import com.android.tradefed.util.CommandStatus;
import com.android.tradefed.util.IRunUtil;
import com.android.tradefed.util.RunUtil;
import com.android.tradefed.util.TimeUtil;

import org.junit.Assert;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//TODO(ghliu): remove this class once we can build and run self-contained par executables
/**
 * Runs Python ARchive (PAR) tests written with the unittest library.
 *
 * <p>Forked from PythonUnitTestRunner as a temporary workaround while waiting for the support of
 * building self-contained par executables
 */
@OptionClass(alias = "par-unit")
public class AdbParUnitTestRunner implements IRemoteTest, IBuildReceiver {

    @Option(
        name = "partest",
        description = "names of python archive (par) modules containing the test cases"
    )
    private List<String> mParTests = new ArrayList<>();

    @Option(name = "min-python-version", description = "minimum required python version")
    private String mMinPyVersion = "2.7.0";

    @Option(name = "python-binary", description = "python binary to use (optional)")
    private String mPythonBin;

    @Option(
        name = "test-timeout",
        description = "maximum amount of time tests are allowed to run",
        isTimeVal = true
    )
    private long mTestTimeout = 1000 * 60 * 5;

    private IBuildInfo mBuildInfo;
    private IRunUtil mRunUtil;
    private StringBuilder mRunUtilPath = new StringBuilder();

    private static final String PATH = "PATH";
    private static final String PYTHONPATH = "PYTHONPATH";
    private static final String VERSION_REGEX = "(?:(\\d+)\\.)?(?:(\\d+)\\.)?(\\*|\\d+)$";

    /** Returns an {@link IRunUtil} that runs the unittest */
    protected IRunUtil getRunUtil() {
        if (mRunUtil == null) {
            mRunUtil = new RunUtil();
        }
        return mRunUtil;
    }

    /** A workaround to allow subclass appending stuff to PATH */
    void appendToPath(String value) {
        if (mRunUtilPath.length() != 0) {
            mRunUtilPath.append(":");
        }
        mRunUtilPath.append(value);
    }

    @Override
    public void run(ITestInvocationListener listener) throws DeviceNotAvailableException {
        // set $PYTHONPATH
        String pythonPath = System.getenv(PYTHONPATH);
        getRunUtil().setEnvVariable(PYTHONPATH, pythonPath);

        // set $PATH
        if (mPythonBin == null) {
            mPythonBin = getPythonBinary();
        }
        String pythonBinDir = new File(mPythonBin).getParent();
        appendToPath(pythonBinDir);
        getRunUtil().setEnvVariable(PATH, mRunUtilPath.toString());

        // run tests
        for (String parTest : mParTests) {
            for (VersionedFile f : mBuildInfo.getFiles()) {
                if (f.getFile().getName().startsWith(parTest)) {
                    File parTestFile = f.getFile();
                    parTestFile.setExecutable(true);
                    executeAndParse(listener, getRunUtil(), parTestFile);
                }
            }
        }
    }

    @Override
    public void setBuild(IBuildInfo buildInfo) {
        mBuildInfo = buildInfo;
    }

    /** Returns the {@link IBuildInfo} for this invocation. */
    protected IBuildInfo getBuild() {
        return mBuildInfo;
    }

    private String getPythonBinary() {
        IRunUtil runUtil = RunUtil.getDefault();
        CommandResult c = runUtil.runTimedCmd(1000, "which", "python");
        String pythonBin = c.getStdout().trim();
        if (pythonBin.length() == 0) {
            throw new RuntimeException("Could not find python binary on host machine");
        }
        c = runUtil.runTimedCmd(1000, pythonBin, "--version");
        // python --version prints to stderr
        CLog.i("Found python version: %s", c.getStderr());
        checkPythonVersion(c);
        return pythonBin;
    }

    protected void checkPythonVersion(CommandResult c) {
        Matcher minVersionParts = Pattern.compile(VERSION_REGEX).matcher(mMinPyVersion);
        Matcher versionParts = Pattern.compile(VERSION_REGEX).matcher(c.getStderr());

        Assert.assertTrue(minVersionParts.find());
        int major = Integer.parseInt(minVersionParts.group(1));
        int minor = Integer.parseInt(minVersionParts.group(2));
        int revision = Integer.parseInt(minVersionParts.group(3));

        Assert.assertTrue(versionParts.find());
        int foundMajor = Integer.parseInt(versionParts.group(1));
        int foundMinor = Integer.parseInt(versionParts.group(2));
        int foundRevision = Integer.parseInt(versionParts.group(3));

        Assert.assertTrue(foundMajor >= major);
        if (!(foundMajor > major)) {
            Assert.assertTrue(foundMinor >= minor);
            if (!(foundMinor > minor)) {
                Assert.assertTrue(foundRevision >= revision);
            }
        }
    }

    private void executeAndParse(ITestRunListener listener, IRunUtil runUtil, File parFile) {
        CommandResult c = runUtil.runTimedCmd(mTestTimeout, parFile.getAbsolutePath());
        if (c.getStatus() == CommandStatus.TIMED_OUT) {
            CLog.e("Python process timed out");
            CLog.e("Stderr: %s", c.getStderr());
            CLog.e("Stdout: %s", c.getStdout());
            throw new RuntimeException(
                    String.format(
                            "Python unit test timed out after %s",
                            TimeUtil.formatElapsedTime(mTestTimeout)));
        }
        // If test execution succeeds, regardless of test results the parser will parse the output.
        // If test execution fails, result parser will throw an exception.
        CLog.i("Parsing test result: %s", c.getStderr());
        MultiLineReceiver parser =
                new PythonUnitTestResultParser(ArrayUtil.list(listener), parFile.getName());
        parser.processNewLines(c.getStderr().split("\n"));
    }
}
