// Copyright 2017 Google Inc. All Rights Reserved.
package com.google.android.tradefed.dexoptota;

import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.result.ITestInvocationListener;
import com.android.tradefed.testtype.IDeviceTest;
import com.android.tradefed.testtype.IRemoteTest;

/**
 * Reboots the device for OTA. This class cannot be a TargetPreparer since all TargetPreparers run
 * before all tests.
 */
public class OtaRebootAction implements IRemoteTest, IDeviceTest {

    private ITestDevice mDevice;

    @Override
    public void setDevice(ITestDevice device) {
        mDevice = device;
    }

    @Override
    public ITestDevice getDevice() {
        return mDevice;
    }

    @Override
    public void run(ITestInvocationListener listener) throws DeviceNotAvailableException {
        mDevice.waitForDeviceAvailable();

        // Set property for testing OTA
        mDevice.executeShellCommand("setprop persist.pm.mock-upgrade true");

        mDevice.reboot();

        // clean up the persistent property
        mDevice.executeShellCommand("rm /data/property/persist.pm.mock-upgrade");
    }
}
