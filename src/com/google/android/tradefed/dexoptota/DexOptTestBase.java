// Copyright 2017 Google Inc. All Rights Reserved.
package com.google.android.tradefed.dexoptota;

import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.testtype.IDeviceTest;

import org.junit.Before;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DexOptTestBase implements IDeviceTest {

    private ITestDevice mDevice;

    static final String APP_DIR = "/data/app";
    static final String DALVIK_CACHE_DIR = "/data/dalvik-cache";
    static final String DALVIK_CACHE_A_DIR = "/data/ota/_a/dalvik-cache";
    static final String DALVIK_CACHE_B_DIR = "/data/ota/_b/dalvik-cache";
    static final String SLOT_A = "_a";
    static final String SLOT_B = "_b";

    protected String mAbOtaFilter;
    protected String mSharedFilter;

    @Override
    public void setDevice(ITestDevice device) {
        mDevice = device;
    }

    @Override
    public ITestDevice getDevice() {
        return mDevice;
    }

    @Before
    public void setUp() throws DeviceNotAvailableException {
        mDevice.enableAdbRoot();
        mAbOtaFilter = mDevice.getProperty("pm.dexopt.ab-ota").trim();
        mSharedFilter = mDevice.getProperty("pm.dexopt.shared").trim();
    }

    protected List<String> findFiles(String dir, String pattern)
            throws DeviceNotAvailableException {
        // For simplicity we assume that filenames don't contain \n
        String cmd = "find " + dir + " -name " + pattern;
        String result = mDevice.executeShellCommand(cmd);
        if (result.isEmpty() || result.contains("No such file or directory")) {
            return new ArrayList<>();
        }
        return Arrays.asList(result.split("\n"));
    }
}
