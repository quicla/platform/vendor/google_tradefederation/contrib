// Copyright 2017 Google Inc. All Rights Reserved.
package com.google.android.tradefed.dexoptota;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.log.LogUtil;
import com.android.tradefed.testtype.DeviceJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Tests for PreOpt during OTA. Since we OTA to the same version, we assume that the packages remain
 * the same. We then compare that the newly created dex files match the existing files, and we check
 * that the files have the correct compiler filter.
 */
@RunWith(DeviceJUnit4ClassRunner.class)
public class PreRebootTest extends DexOptTestBase {

    private static final String DEVICE_TEMP_DIR = "/data/tmp/";
    private static final Pattern COMPILER_FILTER_REGEX =
            Pattern.compile("\ncompiler-filter = (.*)\n");

    private String mNewSlot = null;
    private String mDalvikOtaDir = null;

    private static final List<String> APP_EXCLUSIONS =
            Arrays.asList(
                    "app_chimera", // Special exception
                    "YouTube", // TODO(b/69483677): Decide how to handle YouTube and below
                    "OBDM_Permissions",
                    "NexusWallpapersStubPrebuilt",
                    "PlayAutoInstallConfig");

    interface StringOperation {
        String transform(String old);
    }

    @Override
    @Before
    public void setUp() throws DeviceNotAvailableException {
        super.setUp();
        String currentSlot = getDevice().getProperty("ro.boot.slot_suffix").trim();
        if (SLOT_A.equals(currentSlot)) {
            mNewSlot = SLOT_B;
            mDalvikOtaDir = DALVIK_CACHE_B_DIR;
        } else if (SLOT_B.equals(currentSlot)) {
            mNewSlot = SLOT_A;
            mDalvikOtaDir = DALVIK_CACHE_A_DIR;
        } else {
            fail("Unknown result for ro.boot.slot_suffix: " + currentSlot);
        }
    }

    private void verifyCompilerFilter(StringBuilder errorBuilder, String odexFile)
            throws DeviceNotAvailableException {
        String cmd = "oatdump --oat-file=" + odexFile + " --header-only";
        String dump = getDevice().executeShellCommand(cmd);
        Matcher matcher = COMPILER_FILTER_REGEX.matcher(dump);
        if (matcher.find()) {
            String compilerFilter = matcher.group(1);
            if (!(mAbOtaFilter.equals(compilerFilter) || mSharedFilter.equals(compilerFilter))) {
                errorBuilder.append(odexFile + " compiler filter is " + compilerFilter + "\n");
            }
        } else {
            errorBuilder.append("No compiler filter for " + odexFile + "\n");
        }
    }

    private static boolean isAppExcluded(String app) {
        for (String excluded : APP_EXCLUSIONS) {
            if (app.contains(excluded)) {
                LogUtil.CLog.i("Excluded from file check: " + app);
                return true;
            }
        }
        return false;
    }

    /**
     * Verifies that each oldFile, after running the StringOperation on it, exists in newFiles.
     * Excludes files in APP_EXCLUSIONS.
     */
    private void verifyExpectedFiles(
            List<String> oldFiles, List<String> newFiles, StringOperation op) {
        StringBuilder errorBuilder = new StringBuilder();
        for (String oldFile : oldFiles) {
            String expected = op.transform(oldFile);
            if (!newFiles.contains(expected)) {
                if (isAppExcluded(oldFile)) continue;
                errorBuilder.append("Missing: ").append(expected).append("\n");
            }
        }
        assertTrue(errorBuilder.toString(), errorBuilder.length() == 0);
    }

    /**
     * Check that the new dex files created during PreOpt match the existing dex files in the app
     * cache and dalvik cache. We can assume they're the same since we OTA to the same version.
     */
    @Test
    public void testAppOdexCreated() throws DeviceNotAvailableException {
        verifyExpectedFiles(
                findFiles(APP_DIR, "*.odex"),
                findFiles(APP_DIR, "*.odex." + mNewSlot),
                (old) -> old + "." + mNewSlot);
    }

    @Test
    public void testAppVdexCreated() throws DeviceNotAvailableException {
        verifyExpectedFiles(
                findFiles(APP_DIR, "*.vdex"),
                findFiles(APP_DIR, "*.vdex." + mNewSlot),
                (old) -> old + "." + mNewSlot);
    }

    @Test
    public void testDalvikOatCreated() throws DeviceNotAvailableException {
        verifyExpectedFiles(
                findFiles(DALVIK_CACHE_DIR, "*.oat"),
                findFiles(mDalvikOtaDir, "*.oat"),
                (old) -> old.replace(DALVIK_CACHE_DIR, mDalvikOtaDir));
    }

    @Test
    public void testDalvikDexCreated() throws DeviceNotAvailableException {
        verifyExpectedFiles(
                findFiles(DALVIK_CACHE_DIR, "*.dex"),
                findFiles(mDalvikOtaDir, "*.dex"),
                (old) -> old.replace(DALVIK_CACHE_DIR, mDalvikOtaDir));
    }

    @Test
    public void testDalvikOdexCreated() throws DeviceNotAvailableException {
        verifyExpectedFiles(
                findFiles(DALVIK_CACHE_DIR, "*.odex"),
                findFiles(mDalvikOtaDir, "*.odex"),
                (old) -> old.replace(DALVIK_CACHE_DIR, mDalvikOtaDir));
    }

    @Test
    public void testDalvikVdexCreated() throws DeviceNotAvailableException {
        verifyExpectedFiles(
                findFiles(DALVIK_CACHE_DIR, "*.vdex"),
                findFiles(mDalvikOtaDir, "*.vdex"),
                (old) -> old.replace(DALVIK_CACHE_DIR, mDalvikOtaDir));
    }

    //TODO: Test for .art files

    /**
     * Test that the newly created OTA dex files have compiler filters equal to what is defined in
     * the system properties 'pm.dexopt.ab-ota' or 'pm.dexopt.shared'.
     */
    @Test
    public void testAppCompilerFilter() throws DeviceNotAvailableException {
        StringBuilder errorBuilder = new StringBuilder();
        List<String> newAppOdexFiles = findFiles(APP_DIR, "*.odex." + mNewSlot);
        getDevice().executeShellCommand("mkdir " + DEVICE_TEMP_DIR);
        for (String dexFile : newAppOdexFiles) {
            // oatdump can't handle files that end with '._a', so we make a copy of the file and
            // name it temp.odex
            getDevice().executeShellCommand("cp " + dexFile + " " + DEVICE_TEMP_DIR + "temp.odex");
            String cmd = "cp " + dexFile.replace(".odex.", ".vdex.");
            cmd += " " + DEVICE_TEMP_DIR + "temp.vdex";
            getDevice().executeShellCommand(cmd);
            verifyCompilerFilter(errorBuilder, DEVICE_TEMP_DIR + "temp.odex");
            getDevice().executeShellCommand("rm " + DEVICE_TEMP_DIR + "temp.odex");
            getDevice().executeShellCommand("rm " + DEVICE_TEMP_DIR + "temp.vdex");
        }
        getDevice().executeShellCommand("rm -rf " + DEVICE_TEMP_DIR);
        assertTrue(errorBuilder.toString(), errorBuilder.length() == 0);
    }

    @Test
    public void testDalvikCompilerFilter() throws DeviceNotAvailableException {
        StringBuilder errorBuilder = new StringBuilder();
        List<String> newDalvikOatFiles = new ArrayList<>();
        newDalvikOatFiles.addAll(findFiles(mDalvikOtaDir, "*.oat"));
        newDalvikOatFiles.addAll(findFiles(mDalvikOtaDir, "*.dex"));
        for (String dexFile : newDalvikOatFiles) {
            verifyCompilerFilter(errorBuilder, dexFile);
        }
        assertTrue(errorBuilder.toString(), errorBuilder.length() == 0);
    }
}
