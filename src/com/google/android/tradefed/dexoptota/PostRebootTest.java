// Copyright 2017 Google Inc. All Rights Reserved.
package com.google.android.tradefed.dexoptota;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.testtype.DeviceJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Test that dex and oat files are properly migrated after OTA. We check that the OTA files created
 * by PreOpt no longer exist. We check that the packages are up to date and have the correct
 * compiler filter.
 */
@RunWith(DeviceJUnit4ClassRunner.class)
public class PostRebootTest extends DexOptTestBase {

    private static final Pattern PACKAGE_REGEX = Pattern.compile("\\s*\\[(.+)]");
    private static final Pattern PATH_REGEX = Pattern.compile("\\s*path:\\s+(.+)");
    private static final Pattern DEX_REGEX =
            Pattern.compile("\\s*([^:]+):\\s+([^\\[]+)\\[([^]]+)]");
    private static final Pattern USED_BY_OTHER_REGEX =
            Pattern.compile("\\s*used\\sby\\sother\\sapps:\\s+\\[(.+)]");

    private class DexFile {
        String filename;
        String status;
        String compilationFilter;

        DexFile(String filename) {
            this.filename = filename;
        }
    }

    private class AppPackage {
        String name;
        List<DexFile> dexFiles = new ArrayList<>();
        boolean usedByOther = false;

        AppPackage(String name) {
            this.name = name;
        }

        void verifyPackage(StringBuilder errorBuilder) {
            for (DexFile dexFile : dexFiles) {
                if (!"kOatUpToDate".equals(dexFile.status)) {
                    errorBuilder.append(dexFile.filename);
                    errorBuilder.append(" status is ");
                    errorBuilder.append(dexFile.status);
                    errorBuilder.append("\n");
                }
                if (dexFile.filename.startsWith("/system")) {
                    // Do nothing, these files can have any compilation filter
                } else if (dexFile.filename.contains("/vendor@")) {
                    // Known bug with vendor apps (b/68826235)
                } else if (usedByOther) {
                    if (!mSharedFilter.equals(dexFile.compilationFilter)) {
                        errorBuilder.append(dexFile.filename);
                        errorBuilder.append(" is used_by_other but is ");
                        errorBuilder.append(dexFile.compilationFilter);
                        errorBuilder.append("\n");
                    }
                } else {
                    if (!mAbOtaFilter.equals(dexFile.compilationFilter)) {
                        errorBuilder.append(dexFile.filename);
                        errorBuilder.append(" is not used_by_other but is ");
                        errorBuilder.append(dexFile.compilationFilter);
                        errorBuilder.append("\n");
                    }
                }
            }
        }
    }

    /**
     * Test that the packages on the device have the kOatUpToDate and have the correct compiler
     * filters. Shared packages, denoted by having the 'used by other' property, should have the
     * compiler filter defined in the 'pm.dexopt.shared' system property. Otherwise, the compiler
     * filter should be what is defined in 'pm.dexopt.ab-ota'. Packages located in /system cannot be
     * modified, so will retain the compiler filter they initially had.
     */
    @Test
    public void testVerifyDump() throws Exception {
        StringBuilder errorBuilder = new StringBuilder();
        String[] dump = getDevice().executeShellCommand("dumpsys package dexopt").split("\n");
        AppPackage curPackage = null;
        Matcher matcher;
        for (String line : dump) {
            matcher = PACKAGE_REGEX.matcher(line);
            if (matcher.matches()) {
                if (curPackage != null) {
                    curPackage.verifyPackage(errorBuilder);
                }
                curPackage = new AppPackage(matcher.group(1));
                continue;
            }
            matcher = DEX_REGEX.matcher(line);
            if (matcher.matches()) {
                DexFile dex = new DexFile(matcher.group(2));
                String[] props = matcher.group(3).split(",");
                for (String prop : props) {
                    String[] vals = prop.trim().split("=");
                    if ("compilation_filter".equals(vals[0])) {
                        dex.compilationFilter = vals[1];
                    } else if ("status".equals(vals[0])) {
                        dex.status = vals[1];
                    }
                }
                curPackage.dexFiles.add(dex);
                continue;
            }
            matcher = USED_BY_OTHER_REGEX.matcher(line);
            if (matcher.matches()) {
                curPackage.usedByOther = true;
            }
        }
        assertTrue(errorBuilder.toString(), errorBuilder.length() == 0);
    }

    private void checkAppFilesGone(String pattern) throws DeviceNotAvailableException {
        List<String> files = findFiles(APP_DIR, pattern);
        if (files.size() != 0) {
            fail("a/b OTA files remain:\n" + String.join("\n", files) + "\n");
        }
    }

    /**
     * Check that the move operation on reboot was successful by making sure the temporary OTA files
     * are gone.
     */
    @Test
    public void testAppOdexAGone() throws DeviceNotAvailableException {
        checkAppFilesGone("*.odex._a");
    }

    @Test
    public void testAppOdexBGone() throws DeviceNotAvailableException {
        checkAppFilesGone("*.odex._b");
    }

    @Test
    public void testAppVdexAGone() throws DeviceNotAvailableException {
        checkAppFilesGone("*.vdex._a");
    }

    @Test
    public void testAppVdexBGone() throws DeviceNotAvailableException {
        checkAppFilesGone("*.vdex._b");
    }

    //TODO: Test for .art files

    @Test
    public void testDalvikOtaFilesGone() throws Exception {
        String dataOtaFiles = getDevice().executeShellCommand("ls /data/ota");
        if (!dataOtaFiles.isEmpty()) {
            fail("a/b OTA files remain:\n" + getDevice().executeShellCommand("ls -R /data/ota"));
        }
    }
}
