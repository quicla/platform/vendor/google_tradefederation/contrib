/*
 * Copyright (C) 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.android.tradefed.targetprep;

import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.targetprep.BuildError;
import com.android.tradefed.targetprep.ITargetPreparer;
import com.android.tradefed.targetprep.InstallApkSetup;
import com.android.tradefed.targetprep.TargetSetupError;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A {@link ITargetPreparer} that installs one Play Store apk located on x20 folder.
 *
 * <p>All daily builds of Play Store will be stored under x20 folder: /teams/play-apps-store/apks.
 * Depends on which type of key it has, the build would be in subfolder "dev-keys" or "prod-keys".
 * User could specify key type in option. This target preparer will find the right app to install on
 * device based on options provided. The apk parent folder path follows
 * "PlayStoreClassic.<build_date>.<two_digits_ext>.<two_digits_ext>" pattern.
 */
@OptionClass(alias = "install-playstore-apk")
public class InstallPlayStoreApkSetup extends InstallApkSetup {
    private static final String PROD_KEYS = "prod-keys";
    private static final String DEV_KEYS = "dev-keys";
    private static final String BUILD_FILE_NAME_FORMAT = "phonesky_%s_%s_%s.apk";
    private static final Pattern BUILD_DIR_NAME_PATTERN =
            Pattern.compile("PlayStoreClassic\\.(\\d{8})\\.(\\d{2})\\.(\\d{2}).*");

    @Option(
        name = "parent-folder-name",
        description =
                "Parent folder path for builds."
                        + "Default path is /google/data/ro/teams/play-apps-store/apks"
    )
    private String mParentFolderName = "/google/data/ro/teams/play-apps-store/apks";

    @Option(
        name = "prod-keys",
        description =
                "If install prod-key apks. Default to false."
                        + "If set to false, will install dev-keys apks."
    )
    private Boolean mProdKey = false;

    @Option(
        name = "latest",
        description =
                "Whether fetch latest build. Default to false. If set to true, "
                        + "will overwrite \"build-date\", \"build-subfolder-name\" options "
                        + "and fetch the latest build."
    )
    private Boolean mLatest = false;

    @Option(
        name = "build-subfolder-name",
        description =
                "Folder name to look for apk. Eg. \"PlayStoreClassic.20171125.01.00\". "
                        + "Default to null. If set to null, will check \"build-date\" option."
    )
    private String mBuildFolderName = null;

    @Option(
        name = "build-date",
        description =
                "String for build date. Format should be YYYYMMDD. Default to Null. "
                        + "If multiple builds on that date, will check option \"build-extension\"."
                        + "If both build-date and build-subfolder-name set to null,"
                        + "will fetch the latest one."
    )
    private String mBuildDate = null;

    @Option(
        name = "build-extension",
        description =
                "String for build folder extension. Only check this option when option "
                        + "\"build-date\" is not null and there are multiple builds for that date. "
                        + "Format should be NN.NN. eg. 00.00"
    )
    private String mBuildExt = null;

    @Option(
        name = "type",
        description =
                "Build type eg. classic, headless, lemon, sidewinder..." + "Default to classic."
    )
    private String mBuildType = "classic";

    @Option(
        name = "feature-preview",
        description =
                "Whether take feature preview build. Default to false. Feature preview build name "
                        + "like \"phonesky_classic_featurepreview_all.apk\"."
    )
    private boolean mFeaturePreview = false;

    @Option(
        name = "xhdpi",
        description =
                "Whether take xhdpi build. Default to false. "
                        + "Xhdpi build name ends with \"xhdpi\". "
                        + "If set to false, will fetch build ends with \"all\"."
    )
    private boolean mXhdpi = false;

    @Override
    public void setUp(ITestDevice device, IBuildInfo buildInfo)
            throws TargetSetupError, BuildError, DeviceNotAvailableException {
        if (isDisabled()) {
            CLog.d("This target preparer has been disabled.");
            return;
        }
        Path parentFolderPath = Paths.get(mParentFolderName);
        if (!Files.exists(parentFolderPath)) {
            throw new TargetSetupError(
                    String.format("Cannot find x20 parent path: %s", parentFolderPath.toString()),
                    device.getDeviceDescriptor());
        }
        CLog.d("X20 parent path: ", parentFolderPath.toString());
        Path apkPath = parseTargetApkPath(device);
        getApkPaths().add(apkPath.toFile());
        super.setUp(device, buildInfo);
    }

    /**
     * Get Play store apk path according to options.
     *
     * @param device DUT
     * @return Play store apk path
     * @throws TargetSetupError
     */
    private Path parseTargetApkPath(ITestDevice device) throws TargetSetupError {
        Path folderPath = Paths.get(mParentFolderName, mProdKey ? PROD_KEYS : DEV_KEYS);
        Path buildFolderPath = null;
        if (!mLatest && mBuildFolderName != null) {
            buildFolderPath = folderPath.resolve(mBuildFolderName);
        } else if (!mLatest && mBuildDate != null) {
            buildFolderPath = findBuildPath(folderPath, mBuildDate, mBuildExt, device);
        } else {
            buildFolderPath = getLatestBuildPath(folderPath, device);
        }
        CLog.d("X20 build folder path: " + buildFolderPath.toString());
        Path apkPath = buildFolderPath.resolve(getApkFileName());
        CLog.d("X20 build apk file path: " + apkPath.toString());
        return apkPath;
    }

    private String getApkFileName() {
        String prodOrPreview = mFeaturePreview ? "featurepreview" : "prod";
        String xhdpiOrAll = mXhdpi ? "xhdpi" : "all";
        return String.format(BUILD_FILE_NAME_FORMAT, mBuildType, prodOrPreview, xhdpiOrAll);
    }

    /**
     * Search the whole directory (eg. /google/data/ro/teams/play-apps-store/apks/dev-keys) and
     * return the most recent build path.
     *
     * @param dir Path containing all build directories.
     * @param device
     * @return
     * @throws TargetSetupError
     */
    private Path getLatestBuildPath(Path dir, ITestDevice device) throws TargetSetupError {
        Path latestPath = null;
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
            for (Path buildPath : stream) {
                Matcher matcher =
                        BUILD_DIR_NAME_PATTERN.matcher(buildPath.getFileName().toString());
                if (matcher.matches()) {
                    latestPath = compareBuild(latestPath, buildPath);
                }
            }
        } catch (IOException e) {
            throw new TargetSetupError(
                    String.format("Fail to find latest build path."), device.getDeviceDescriptor());
        }
        CLog.d("Find latest build path: " + latestPath.toString());
        return latestPath;
    }

    /**
     * Search the directory for build path which matches specified build date and extension.
     *
     * @param dir Path containing all build directories.
     * @param buildDate string for build date, format YYYYMMDD
     * @param buildExt string for build extension format NN.NN
     * @param device DUT
     * @return
     * @throws TargetSetupError
     */
    private Path findBuildPath(Path dir, String buildDate, String buildExt, ITestDevice device)
            throws TargetSetupError {
        Path resultPath = null;
        DirectoryStream.Filter<Path> filter =
                new DirectoryStream.Filter<Path>() {
                    @Override
                    public boolean accept(Path entry) throws IOException {
                        Matcher matcher =
                                BUILD_DIR_NAME_PATTERN.matcher(entry.getFileName().toString());
                        if (matcher.matches()) {
                            return matcher.group(1).equals(buildDate);
                        }
                        return false;
                    }
                };
        Path maxBuild = null;
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, filter)) {
            for (Path buildPath : stream) {
                if (buildExt != null && buildPath.endsWith(buildExt)) {
                    resultPath = buildPath;
                    break;
                }
                maxBuild = compareBuild(maxBuild, buildPath);
            }
        } catch (IOException e) {
            throw new TargetSetupError(
                    String.format(
                            "Failed to find build path for date %s with extension %s.",
                            buildDate, buildExt),
                    device.getDeviceDescriptor());
        }
        if (resultPath == null) {
            resultPath = maxBuild;
        }
        CLog.d(
                String.format(
                        "Find build path with date %s and ext %s: %s",
                        buildDate, buildExt, resultPath));
        return resultPath;
    }

    /**
     * Compare build directories, return more recent one. To decide which one is more recent, it
     * will parse the directory name, which contains build date and extension. Build directory name
     * example: PlayStoreClassic.20171011.01.00
     *
     * @param path1
     * @param path2
     * @return
     */
    private Path compareBuild(Path path1, Path path2) {
        if (path1 == null || path2 == null) {
            return path1 == null ? path2 : path1;
        }
        Matcher matcher1 = BUILD_DIR_NAME_PATTERN.matcher(path1.getFileName().toString());
        Matcher matcher2 = BUILD_DIR_NAME_PATTERN.matcher(path2.getFileName().toString());
        if (!matcher1.matches() || !matcher2.matches()) {
            return matcher1.matches() ? path1 : path2;
        }
        for (int ind = 1; ind <= matcher1.groupCount(); ind++) {
            int v1 = Integer.valueOf(matcher1.group(ind));
            int v2 = Integer.valueOf(matcher2.group(ind));
            if (v1 == v2) {
                continue;
            }
            return v1 > v2 ? path1 : path2;
        }
        return path1;
    }
}
