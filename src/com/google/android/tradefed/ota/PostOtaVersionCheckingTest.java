package com.google.android.tradefed.ota;

import com.android.ddmlib.testrunner.TestIdentifier;
import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.build.IDeviceBuildInfo;
import com.android.tradefed.build.OtaDeviceBuildInfo;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.result.ITestInvocationListener;
import com.android.tradefed.testtype.IBuildReceiver;
import com.android.tradefed.testtype.IDeviceTest;
import com.android.tradefed.testtype.IRemoteTest;

import com.google.common.annotations.VisibleForTesting;

import java.util.Collections;

/** Check if build id, bootloader and baseband were correctly updated after an OTA. */
@OptionClass(alias = "post-ota-version-check")
public class PostOtaVersionCheckingTest implements IDeviceTest, IRemoteTest, IBuildReceiver {

    private static final String TEST_RUN_NAME = "PostOtaVersionCheckingTest";
    private static final String CLASS_NAME = "PostOtaVersionCheckingTest";

    private ITestDevice mDevice;
    private IBuildInfo mBuildInfo;
    private ITestInvocationListener mListener;

    /** {@inheritDoc} */
    @Override
    public void setDevice(ITestDevice device) {
        mDevice = device;
    }

    /** {@inheritDoc} */
    @Override
    public ITestDevice getDevice() {
        return mDevice;
    }

    /** {@inheritDoc} */
    @Override
    public void setBuild(IBuildInfo buildInfo) {
        mBuildInfo = buildInfo;
    }

    public PostOtaVersionCheckingTest() {}

    @VisibleForTesting
    public PostOtaVersionCheckingTest(ITestInvocationListener listener) {
        mListener = listener;
    }

    /** {@inheritDoc} */
    @Override
    public void run(ITestInvocationListener listener) throws DeviceNotAvailableException {
        mListener = listener;

        IDeviceBuildInfo expectedBuild = ((OtaDeviceBuildInfo) mBuildInfo).getOtaBuild();
        String expectedBuildId = expectedBuild.getBuildId();
        String expectedBootloader = expectedBuild.getBootloaderVersion(); // may be null
        String expectedBaseband = expectedBuild.getBasebandVersion(); // may be null

        String actualBuildId = mDevice.getBuildId();
        String actualBootloader = mDevice.getBootloaderVersion();
        String actualBaseBand = mDevice.getBasebandVersion(); // may be null

        // runName passed to testRunStarted -> Sponge top level "target"
        listener.testRunStarted(TEST_RUN_NAME, 3);
        checkVersionAndReport("CheckBuildId", expectedBuildId, actualBuildId, false);
        checkVersionAndReport("CheckBootloaderVersion", expectedBootloader, actualBootloader, true);
        checkVersionAndReport("CheckBasebandVersion", expectedBaseband, actualBaseBand, true);
        listener.testRunEnded(0, Collections.emptyMap());
    }

    /**
     * Helper function to compare versions and report to listener.
     *
     * <p>The test will be ignored if the expected value is null, or (only when it's skippable) if
     * the actual value is null.
     */
    @VisibleForTesting
    void checkVersionAndReport(String testName, String expected, String actual, boolean skippable) {
        // className passed to TestIdentifier -> BlackBox result group (with a page icon)
        // testName passed to TestIdentifier -> BlackBox result item (with a dot icon)
        TestIdentifier testId =
                new TestIdentifier(mBuildInfo.getTestTag(), CLASS_NAME + "." + testName);
        mListener.testStarted(testId);
        if (expected == null) {
            // if expected is null, always skip
            mListener.testIgnored(testId);
        } else if (skippable && actual == null) {
            // if actual is null, only skip when it's allowed
            mListener.testIgnored(testId);
        } else if (!expected.equals(actual)) {
            String failReason =
                    String.format("%s failed: expect %s, found %s", testName, expected, actual);
            mListener.testFailed(testId, failReason);
        }
        mListener.testEnded(testId, Collections.emptyMap());
    }
}
