// Copyright 2017 Google Inc. All Rights Reserved.
package com.google.android.tradefed.ota;

import static com.google.android.tradefed.ota.util.BootctlUtil.getCurrentSlot;
import static com.google.android.tradefed.ota.util.BootctlUtil.isSlotMarkedSuccessful;

import com.android.ddmlib.testrunner.TestIdentifier;
import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.build.OtaDeviceBuildInfo;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.result.ByteArrayInputStreamSource;
import com.android.tradefed.result.ITestInvocationListener;
import com.android.tradefed.result.InputStreamSource;
import com.android.tradefed.result.LogDataType;
import com.android.tradefed.testtype.IBuildReceiver;
import com.android.tradefed.testtype.IDeviceTest;
import com.android.tradefed.testtype.IRemoteTest;
import com.android.tradefed.util.ZipUtil;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.zip.ZipFile;

/** A test for OTA Update Verifier. */
public class UpdateVerifierTest implements IRemoteTest, IDeviceTest, IBuildReceiver {

    private static final String TEST_RUN_NAME = "UpdateVerifierTest";
    private static final String CLASS_NAME = "UpdateVerifierTest";

    private static final String DEVICE_CARE_MAP = "/data/ota_package/care_map.txt";

    private ITestDevice mDevice;
    private OtaDeviceBuildInfo mBuildInfo;

    /** {@inheritDoc} */
    @Override
    public void setDevice(ITestDevice device) {
        mDevice = device;
    }

    /** {@inheritDoc} */
    @Override
    public ITestDevice getDevice() {
        return mDevice;
    }

    /** {@inheritDoc} */
    @Override
    public void setBuild(IBuildInfo buildInfo) {
        mBuildInfo = (OtaDeviceBuildInfo) buildInfo;
    }

    /** {@inheritDoc} */
    @Override
    public void run(ITestInvocationListener listener) throws DeviceNotAvailableException {
        getDevice().enableAdbRoot();
        pushCareMap();
        String expectedBootSlot = clearBootSuccessFlag();
        getDevice().reboot();

        listener.testRunStarted(TEST_RUN_NAME, 2);
        checkBootSlot(expectedBootSlot, listener);
        checkBootSuccessFlag(expectedBootSlot, listener);
        reportUpdateVerifierDmesg(listener);
        listener.testRunEnded(0, Collections.emptyMap());
    }

    /** Extracts care_map.txt and pushes it onto the device. */
    private void pushCareMap() throws DeviceNotAvailableException {
        try {
            File otaPackage = mBuildInfo.getOtaBuild().getOtaPackageFile();
            File careMap = ZipUtil.extractFileFromZip(new ZipFile(otaPackage), "care_map.txt");
            // /data/ota_package is guaranteed to be created by init
            getDevice().executeAdbCommand("push", careMap.getAbsolutePath(), DEVICE_CARE_MAP);
            getDevice().executeShellCommand("chmod 0640 " + DEVICE_CARE_MAP);
            getDevice().executeShellCommand("chgrp cache " + DEVICE_CARE_MAP);
            getDevice()
                    .executeShellCommand("chcon u:object_r:ota_package_file:s0 " + DEVICE_CARE_MAP);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /** Clears the boot success flag and returns the boot slot ("0" or "1"). */
    private String clearBootSuccessFlag() throws DeviceNotAvailableException {
        String bootSlot = getCurrentSlot(getDevice());
        // set the slot the same value - we do this to clear the boot successful flag
        getDevice().executeShellCommand("bootctl set-active-boot-slot " + bootSlot);
        // make sure the flag was cleared
        if (isSlotMarkedSuccessful(getDevice(), bootSlot)) {
            // if the slot *IS* still marked successful, we *FAILED* to clear the flag
            throw new RuntimeException("Failed to clear the boot success flag.");
        }
        return bootSlot;
    }

    /** Tests if the boot slot is as expected. */
    private void checkBootSlot(String expectedSlot, ITestInvocationListener listener)
            throws DeviceNotAvailableException {
        TestIdentifier testId = new TestIdentifier(CLASS_NAME, "CheckBootSlot");
        listener.testStarted(testId);
        if (!expectedSlot.equals(getCurrentSlot(getDevice()))) {
            String msg = String.format("Boot slot was not %s", expectedSlot);
            listener.testFailed(testId, msg);
        }
        listener.testEnded(testId, Collections.emptyMap());
    }

    /** Tests if the expected boot slot was marked success. */
    private void checkBootSuccessFlag(String slot, ITestInvocationListener listener)
            throws DeviceNotAvailableException {
        TestIdentifier testId = new TestIdentifier(CLASS_NAME, "CheckBootSuccessFlag");
        listener.testStarted(testId);
        if (!isSlotMarkedSuccessful(getDevice(), slot)) {
            String msg = String.format("Boot slot %s was not marked successful", slot);
            listener.testFailed(testId, msg);
        }
        listener.testEnded(testId, Collections.emptyMap());
    }

    /** Collects and uploads update_verifier related dmesg. */
    private void reportUpdateVerifierDmesg(ITestInvocationListener listener)
            throws DeviceNotAvailableException {
        String msg = getDevice().executeShellCommand("dmesg | grep -F update_verifier");
        InputStreamSource stream = new ByteArrayInputStreamSource(msg.getBytes());
        listener.testLog("device_dmesg_update_verifier", LogDataType.TEXT, stream);
    }
}
