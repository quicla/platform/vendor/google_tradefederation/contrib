// Copyright 2015 Google Inc.  All Rights Reserved.

package com.google.android.tradefed.ota;

import static com.google.android.tradefed.ota.util.BootctlUtil.getCurrentSlot;
import static com.google.android.tradefed.ota.util.BootctlUtil.isSlotSwapped;
import static com.google.android.tradefed.ota.util.PayloadUtil.Payload;
import static com.google.android.tradefed.ota.util.PayloadUtil.getAndroidUpdateCommand;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.IShellOutputReceiver;
import com.android.ddmlib.MultiLineReceiver;
import com.android.ddmlib.NullOutputReceiver;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.android.ddmlib.testrunner.TestIdentifier;
import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.build.OtaDeviceBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.BackgroundDeviceAction;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ILogcatReceiver;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.device.LogcatReceiver;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.result.ITestInvocationListener;
import com.android.tradefed.testtype.IBuildReceiver;
import com.android.tradefed.testtype.IDeviceTest;
import com.android.tradefed.testtype.IRemoteTest;
import com.android.tradefed.util.ArrayUtil;
import com.android.tradefed.util.LogcatUpdaterEventParser;
import com.android.tradefed.util.StreamUtil;
import com.android.tradefed.util.UpdaterEventType;

import com.google.common.annotations.VisibleForTesting;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/** A test which runs A/B OTA. */
@OptionClass(alias = "ab-ota")
public class OtaFunctionalTest implements IDeviceTest, IRemoteTest, IBuildReceiver {

    public enum OtaType {
        FULL,
        INCREMENTAL,
    }

    private static final String CHECKIN_CMD =
            "am broadcast -a android.server.checkin.CHECKIN com.google.android.gms";
    private static final long LOGCAT_FILE_SIZE = 1024 * 10 * 10;
    private static final int LOGCAT_START_DELAY = 0;
    private static final String LOGCAT_RECEIVER_CMD = "logcat -s";
    @VisibleForTesting
    static final String DEFAULT_PACKAGE_DATA_PATH = "/data/ota_package/update.zip";

    @Option(
        name = "test-ota-type",
        description =
                "Type of OTA upgrade to test; options are FULL and INCREMENTAL; defaults to FULL"
    )
    private OtaType mOtaType = OtaType.FULL;

    @Option(name = "downloaded-build", description = "Whether or not to download the OTA package "
            + "via checkin.")
    private boolean mDownloadedBuild = false;

    @Option(
        name = "package-data-path",
        description = "path on /data for the package to be saved to"
    )
    private String mPackageDataPath = DEFAULT_PACKAGE_DATA_PATH;

    @Option(
        name = "apply-timeout",
        description = "time in milliseconds to wait for ota to finish",
        isTimeVal = true
    )
    private long mUpdateTimeoutMs = 3 * 60 * 60 * 1000;

    @Option(
        name = "echo-uec-output",
        description = "whether or not to print update_engine_client output to the terminal"
    )
    private boolean mEchoUecOutput = true;

    @Option(name = "logcat-spec", description = "tags to include in captured logcat output")
    private List<String> mLogcatSpecs = new ArrayList<>();

    @Option(
        name = "skip-post-ota-reboot",
        description = "whether to skip the reboot and post-reboot tests"
    )
    private Boolean mSkipReboot = false;

    private LogcatUpdaterEventParser mLogcatParser = null;
    private ILogcatReceiver mLogcatReceiver = null;
    private OtaDeviceBuildInfo mOtaDeviceBuild;
    private ITestDevice mDevice;
    private BackgroundDeviceAction mLogcatEchoReceiver;

    private static final String TEST_RUN_NAME = "OtaFunctionalTest";
    private static final String CLASS_NAME = "OtaFunctionalTest";

    /**
     * Callback function to notify subclass of OTA state. Must not block when called with
     * UPDATE_START or PATCH_COMPLETE. If blocking actions must be performed, asynchronous threads
     * should be used.
     *
     * @param event The {@link UpdaterEventType} corresponding to the event.
     *     <pre>
     *     UPDATE_START - Used when update engine starts and patch phase begins.
     *         In this phase, Update Engine reports as UPDATE_STATUS_DOWNLOADING.
     *         Must not block when this is used.
     *     PATCH_COMPLETE - Used when patch phase ends and dexopt phase begins.
     *         In this phase, Update Engine reports as UPDATE_STATUS_FINALIZING.
     *         Must not block when this is used.
     *     UPDATE_COMPLETE - Used when Update Engine is finished.
     *         The function may block to perform extra tests when this is used.
     *         The test will wait and then reboot afterwards (if enabled).
     *     ERROR - Used if an error is encountered with Update Engine.
     *         Any cleanup should be performed at this time.
     *     </pre>
     */
    protected void onEvent(UpdaterEventType event) throws DeviceNotAvailableException {}

    /** Called before execution of OTA Functional Test for subclass to perform setup. */
    protected void preOtaTest() throws DeviceNotAvailableException {}

    /**
     * Called upon successful completion of OTA Functional Test for subclasses to perform cleanup
     * and report results.
     *
     * @param listener The listener to report test results to
     */
    protected void postOtaTest(ITestInvocationListener listener)
            throws DeviceNotAvailableException {}

    /** Creates a {@link TestIdentifier} for an OTA functional test. */
    private TestIdentifier getTestId(String testName) {
        // className passed to TestIdentifier -> BlackBox result group (with a page icon)
        // testName passed to TestIdentifier -> BlackBox result item (with a dot icon)
        return new TestIdentifier(mOtaDeviceBuild.getTestTag(), CLASS_NAME + "." + testName);
    }

    public OtaFunctionalTest() {}

    @VisibleForTesting
    OtaFunctionalTest(ILogcatReceiver receiver) {
        mLogcatReceiver = receiver;
    }

    /** {@inheritDoc} */
    @Override
    public void setDevice(ITestDevice device) {
        mDevice = device;
    }

    /** {@inheritDoc} */
    @Override
    public ITestDevice getDevice() {
        return mDevice;
    }

    /** {@inheritDoc} */
    @Override
    public void setBuild(IBuildInfo buildInfo) {
        mOtaDeviceBuild = (OtaDeviceBuildInfo) buildInfo;
    }

    @VisibleForTesting
    File getOtaPackage(OtaDeviceBuildInfo build, OtaType type) {
        File otaPackage = null;
        switch (type) {
            case FULL:
                otaPackage = build.getOtaBuild().getOtaPackageFile();
                CLog.i("Test full OTA");
                break;
            case INCREMENTAL:
                otaPackage = build.getOtaPackageFile();
                CLog.i("Test incremental OTA");
                break;
        }
        if (otaPackage == null) {
            throw new IllegalStateException("Received an OtaDeviceBuildInfo with no package");
        }
        return otaPackage;
    }

    private void checkin() throws DeviceNotAvailableException {
        getDevice().executeShellCommand(CHECKIN_CMD);
    }

    private void startLogcatListener() {
        if (mLogcatReceiver == null) {
            mLogcatReceiver = new LogcatReceiver(getDevice(), LOGCAT_FILE_SIZE, LOGCAT_START_DELAY);
        }
        if (mLogcatParser == null) {
            mLogcatParser = new LogcatUpdaterEventParser(mLogcatReceiver);
        }
        mLogcatReceiver.start();
    }

    private void stopLogcatListener() {
        if (mLogcatReceiver != null) {
            mLogcatReceiver.stop();
        }
        StreamUtil.close(mLogcatParser);
    }

    private void addDefaultLogcatSpecs() {
        mLogcatSpecs.add("update_engine:I");
        mLogcatSpecs.add("update_engine_client:I");
        mLogcatSpecs.add("CmaSystemUpdateService:I");
        mLogcatSpecs.add("UpdateEngineTask:I");
        mLogcatSpecs.add("SystemUpdateTask:I");
        mLogcatSpecs.add("SystemUpdateClient:I");
    }

    private String getLogcatReceiverCmd() {
        StringBuilder sb = new StringBuilder();
        sb.append(LOGCAT_RECEIVER_CMD);
        sb.append(" ");
        sb.append(ArrayUtil.join(" ", mLogcatSpecs));
        return sb.toString();
    }

    /** Pushes an OTA package and executes update_engine_client command to initiate OTA */
    @VisibleForTesting
    void kickoffSideloadUpdate() throws DeviceNotAvailableException {
        File sideloadPackage = getOtaPackage(mOtaDeviceBuild, mOtaType);
        CLog.i("Pushing OTA package %s", sideloadPackage.getAbsolutePath());
        if (!getDevice().pushFile(sideloadPackage, mPackageDataPath)) {
            throw new RuntimeException("Unable to push file onto device");
        }
        String payloadUrl = "file://" + mPackageDataPath;

        if (mEchoUecOutput) {
            IShellOutputReceiver receiver =
                    new MultiLineReceiver() {
                        @Override
                        public void processNewLines(String[] lines) {
                            for (String l : lines) {
                                CLog.d(l);
                            }
                        }

                        @Override
                        public boolean isCancelled() {
                            return false;
                        }
                    };
            mLogcatEchoReceiver =
                    new BackgroundDeviceAction(
                            getLogcatReceiverCmd(),
                            "UpdateEngineLogcatWatcher",
                            getDevice(),
                            receiver,
                            0);
            mLogcatEchoReceiver.start();
        }
        Payload payload = initPayload(sideloadPackage.getAbsolutePath());
        final String cmd = getAndroidUpdateCommand(payload, payloadUrl);
        Thread uec =
                new Thread(
                        () -> {
                            CLog.d(
                                    "Starting UpdateEngineRunner for %s.",
                                    getDevice().getSerialNumber());
                            try {
                                getDevice()
                                        .getIDevice()
                                        .executeShellCommand(
                                                cmd,
                                                new NullOutputReceiver(),
                                                0,
                                                TimeUnit.MILLISECONDS);
                            } catch (TimeoutException
                                    | AdbCommandRejectedException
                                    | ShellCommandUnresponsiveException
                                    | IOException e) {
                                CLog.e("Received exception from UpdateEngineRunner");
                                CLog.e(e);
                            }
                        });
        uec.setDaemon(true);
        uec.start();
        CLog.i("Running update command %s", cmd);
    }

    /**
     * Triggers an OTA and checks: 1) if update_engine tasks succeeded, and 2) if boot slot was
     * swapped post-reboot.
     *
     * <p>These tests are considered essential to *ANY* OTA tests, even if {@link OtaFunctionalTest}
     * is used for purpose other than testing OTA itself (e.g. data migration, dexopt).
     *
     * <p>{@inheritDoc}
     */
    @Override
    public void run(ITestInvocationListener listener) throws DeviceNotAvailableException {
        long startTimeNano = System.nanoTime();
        int testCount = mSkipReboot ? 1 : 2;
        listener.testRunStarted(TEST_RUN_NAME, testCount);

        preOtaTest();

        // kick off the OTA and update the device
        String slotBefore = getCurrentSlot(getDevice());
        doRunUpdate();

        // consider it successful if we can finish the OTA without exception. For now we always
        // retry when there is *ANY* trouble
        TestIdentifier testIdUpdate = getTestId("TestApplyUpdate");
        listener.testStarted(testIdUpdate);
        listener.testEnded(testIdUpdate, Collections.emptyMap());

        // certain tests need to skip reboot and post-reboot checks
        if (!mSkipReboot) {
            getDevice().reboot();

            // check if the boot slot was swapped
            String slotAfter = getCurrentSlot(getDevice());
            TestIdentifier testIdSlot = getTestId("TestSwapBootSlot");
            listener.testStarted(testIdSlot);
            if (!isSlotSwapped(slotBefore, slotAfter)) {
                String failReason =
                        String.format(
                                "Pre-OTA boot slot: %s, post-OTA boot slot: %s.",
                                slotBefore, slotAfter);
                listener.testFailed(testIdSlot, failReason);
            }
            listener.testEnded(testIdSlot, Collections.emptyMap());
        }

        postOtaTest(listener);

        long timeElapsedMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTimeNano);
        listener.testRunEnded(timeElapsedMs, Collections.emptyMap());
    }

    /** Runs the actual OTA update. */
    private void doRunUpdate() throws DeviceNotAvailableException {
        addDefaultLogcatSpecs();
        startLogcatListener();
        if (mDownloadedBuild) {
            checkin();
        } else {
            kickoffSideloadUpdate();
        }
        UpdaterEventType updateStartResult =
                mLogcatParser.waitForEvent(UpdaterEventType.UPDATE_START, mUpdateTimeoutMs);
        if (!UpdaterEventType.UPDATE_START.equals(updateStartResult)) {
            onEvent(UpdaterEventType.ERROR);
            throw new RuntimeException("OTA Update failed: " + updateStartResult.toString());
        }
        onEvent(UpdaterEventType.UPDATE_START);
        UpdaterEventType patchCompleteResult =
                mLogcatParser.waitForEvent(UpdaterEventType.PATCH_COMPLETE, mUpdateTimeoutMs);
        if (!UpdaterEventType.PATCH_COMPLETE.equals(patchCompleteResult)) {
            onEvent(UpdaterEventType.ERROR);
            throw new RuntimeException("OTA Update failed: " + patchCompleteResult.toString());
        }
        onEvent(UpdaterEventType.PATCH_COMPLETE);
        UpdaterEventType updateCompleteResult =
                mLogcatParser.waitForEvent(UpdaterEventType.UPDATE_COMPLETE, mUpdateTimeoutMs);
        if (!UpdaterEventType.UPDATE_COMPLETE.equals(updateCompleteResult)) {
            onEvent(UpdaterEventType.ERROR);
            throw new RuntimeException("OTA Update failed: " + updateCompleteResult.toString());
        }
        onEvent(UpdaterEventType.UPDATE_COMPLETE);
        stopLogcatListener();
        if (mLogcatEchoReceiver != null) {
            mLogcatEchoReceiver.cancel();
        }
    }

    @VisibleForTesting
    Payload initPayload(final String filename) {
        return new Payload(filename);
    }
}
