// Copyright 2015 Google Inc.  All Rights Reserved.

package com.google.android.tradefed.ota;

import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.build.IDeviceBuildInfo;
import com.android.tradefed.build.OtaDeviceBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.targetprep.BaseTargetPreparer;
import com.android.tradefed.targetprep.BuildError;
import com.android.tradefed.targetprep.TargetSetupError;

/**
 * Stores information about a current or upgrade-targeted version in the device's data fs.
 *
 * @deprecated checking version with instrumentation test is flaky, use {@link
 *     PostOtaVersionCheckingTest} instead
 */
@Deprecated
@OptionClass(alias = "version-cache")
public class VersionCachePreparer extends BaseTargetPreparer {

    @Option(name = "record-slots", description = "whether or not to record slot info")
    private boolean mRecordSlots = false;

    @Option(name = "use-first-version", description = "whether to use the build's starting " +
            "version to record expected values. default is false")
    private boolean mUseFirstVersion = false;

    public static final String OTATEST_BASE_DIR = "/sdcard/otatest/";
    public static final String OTATEST_VERSION_OLD = OTATEST_BASE_DIR + "version.old";
    public static final String OTATEST_VERSION_NEW = OTATEST_BASE_DIR + "version.new";

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp(ITestDevice device, IBuildInfo buildInfo)
            throws TargetSetupError, BuildError, DeviceNotAvailableException {
        IDeviceBuildInfo expectedPostOtaBuild;
        try {
            expectedPostOtaBuild = getPostOtaBuild(buildInfo, mUseFirstVersion);
        } catch (RuntimeException e) {
            throw new TargetSetupError(e.getMessage(), device.getDeviceDescriptor());
        }

        String oldVersionInfo = String.format("%s\n%s\n%s\n", device.getBuildId(),
                device.getBootloaderVersion(), device.getBasebandVersion());
        // not every update will contain bootloader or baseband info, so preserve
        // the old versions in this case
        String newBootloader =
                expectedPostOtaBuild.getBootloaderVersion() != null
                        ? expectedPostOtaBuild.getBootloaderVersion()
                        : device.getBootloaderVersion();
        String newBaseband =
                expectedPostOtaBuild.getBasebandVersion() != null
                        ? expectedPostOtaBuild.getBasebandVersion()
                        : device.getBasebandVersion();
        String newVersionInfo =
                String.format(
                        "%s\n%s\n%s\n",
                        expectedPostOtaBuild.getBuildId(), newBootloader, newBaseband);
        device.executeShellCommand("mkdir " + OTATEST_BASE_DIR);
        device.executeShellCommand(
                String.format("echo \"%s\" > %s", oldVersionInfo, OTATEST_VERSION_OLD));
        device.executeShellCommand(
                String.format("echo \"%s\" > %s", newVersionInfo, OTATEST_VERSION_NEW));
        if (mRecordSlots) {
            device.executeShellCommand(
                    "bootctl get-current-slot > /data/slots.old");
        }
        // ensure that /data will be readable by any InstrumentationTests that try to access it
        device.executeShellCommand("chmod 775 /data");
    }

    public static IDeviceBuildInfo getPostOtaBuild(IBuildInfo buildInfo, boolean useFirstVersion) {
        IDeviceBuildInfo postOtaBuild;
        if (buildInfo instanceof OtaDeviceBuildInfo) {
            OtaDeviceBuildInfo odbi = ((OtaDeviceBuildInfo) buildInfo);
            postOtaBuild = useFirstVersion ? odbi.getBaselineBuild() : odbi.getOtaBuild();
        } else {
            // generic IDeviceBuildInfo only has the "first version"
            postOtaBuild = useFirstVersion ? (IDeviceBuildInfo) buildInfo : null;
        }
        if (postOtaBuild == null) {
            throw new RuntimeException("Failed to get post ota build");
        }
        return postOtaBuild;
    }
}

