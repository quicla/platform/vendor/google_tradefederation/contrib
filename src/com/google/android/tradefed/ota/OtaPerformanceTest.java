// Copyright 2017 Google Inc. All Rights Reserved.
package com.google.android.tradefed.ota;

import static com.android.tradefed.log.LogUtil.CLog;

import com.android.ddmlib.testrunner.TestIdentifier;
import com.android.tradefed.config.Option;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.result.ITestInvocationListener;
import com.android.tradefed.util.UpdaterEventType;

import com.google.common.annotations.VisibleForTesting;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class OtaPerformanceTest extends OtaFunctionalTest {

    private static final String CLASS_NAME = "OtaPerformanceTest";

    static final TestIdentifier TIME_TESTID = new TestIdentifier(CLASS_NAME, "Time (s)");
    static final String TIME_OVERALL_METRICNAME = "Update Engine Overall";
    static final String TIME_PATCH_METRICNAME = "Update Engine Patch";
    static final String TIME_FINALIZE_METRICNAME = "Update Engine Dex2Oat";
    static final TestIdentifier CPU_TESTID = new TestIdentifier(CLASS_NAME, "CPU Usage (%)");
    static final String CPU_OVERALL_METRICNAME = "Update Engine Overall";
    static final String CPU_PATCH_METRICNAME = "Update Engine Patch";
    static final String CPU_FINALIZE_METRICNAME = "Update Engine Dex2Oat";
    static final TestIdentifier MEMORY_TESTID =
            new TestIdentifier(CLASS_NAME, "Max Memory Usage (MB)");
    static final String MEMORY_DELTA_METRICNAME = "OTA Delta";
    static final String MEMORY_TOTAL_METRICNAME = "Device Total";
    static final TestIdentifier TEMPERATURE_TESTID =
            new TestIdentifier(CLASS_NAME, "Max Temperature (C)");
    static final String MAX_TEMPERATURE_METRICNAME = "tsens_tz_sensor0";

    static final double MB_TO_BYTE = 1048576D;
    static final double SECONDS_TO_NANO = 1000 * 1000 * 1000D;

    private ITestDevice mDevice;

    private volatile UpdaterEventType mState = null;

    private long mStartTime;
    private long mPatchCompleteTime;
    private long mOtaCompleteTime;

    private CpuUsage mInitialCpuUsage;
    private CpuUsage mPatchCpuUsage;
    private CpuUsage mOtaCpuUsage;

    private long mInitialMemoryUsage;
    private volatile long mMaxMemoryUsage;

    private String mTemperatureFile = null;
    private long mMaxTemperature = 0;

    @Option(
        name = "ota-performance-interval",
        description = "the interval between collecting OTA performance metrics",
        isTimeVal = true
    )
    private long mIntervalMs = 60 * 1000L;

    private Timer timer;

    static class CpuUsage {
        CpuUsage(long load, long total) {
            this.load = load;
            this.total = total;
        }

        long load;
        long total;

        CpuUsage delta(CpuUsage end) {
            return new CpuUsage(end.load - this.load, end.total - this.total);
        }
    }

    @Override
    protected void onEvent(UpdaterEventType state) throws DeviceNotAvailableException {
        mState = state;
        switch (mState) {
            case UPDATE_START:
                onPatchStart();
                return;
            case PATCH_COMPLETE:
                onDexoptStart();
                return;
            case UPDATE_COMPLETE:
                onUpdateComplete();
                return;
            default:
                onError();
        }
    }

    @Override
    protected void preOtaTest() throws DeviceNotAvailableException {
        mDevice = getDevice();

        // select temperature file
        try {
            // From experiments on Marlin and Walleye, tsens_tz_sensor0 is always hottest.
            String grepResult =
                    mDevice.executeShellCommand(
                                    "grep -F tsens_tz_sensor0 /sys/class/thermal/*/type")
                            .split(":")[0];
            if (grepResult.contains("/sys/class/thermal/")) {
                mTemperatureFile = grepResult.replace("type", "temp");
            } else {
                CLog.i("Temperature measurement not supported for device");
            }
        } catch (DeviceNotAvailableException e) {
            throw new RuntimeException(e);
        }

        // We measure memory usage now to use as a baseline for device idle
        mInitialMemoryUsage = getMemoryUsage();
        // We do not measure CPU usage now since we do not want to include the adb push of the image

        timer = new Timer("OtaPerformanceTest Metric Collector", true);
        TimerTask timerTask =
                new TimerTask() {
                    @Override
                    public void run() {
                        collectMetrics();
                    }
                };

        timer.schedule(timerTask, 0, mIntervalMs);
    }

    private void onPatchStart() throws DeviceNotAvailableException {
        mInitialCpuUsage = getCpuUsage();
        mStartTime = getTimeNano();
    }

    private void onDexoptStart() throws DeviceNotAvailableException {
        mPatchCompleteTime = getTimeNano();
        mPatchCpuUsage = getCpuUsage();
    }

    private void onUpdateComplete() throws DeviceNotAvailableException {
        mOtaCompleteTime = getTimeNano();
        mOtaCpuUsage = getCpuUsage();
        timer.cancel();
    }

    private void onError() throws DeviceNotAvailableException {
        if (timer != null) {
            timer.cancel();
        }
    }

    @Override
    protected void postOtaTest(ITestInvocationListener listener) {
        if (mState != UpdaterEventType.UPDATE_COMPLETE) {
            // Something went wrong, data will not be reliable
            CLog.e("Didn't detect OTA Complete");
            return;
        }

        long totalTime = mOtaCompleteTime - mStartTime;
        long patchTime = mPatchCompleteTime - mStartTime;
        long d2oTime = mOtaCompleteTime - mPatchCompleteTime;

        CpuUsage totalCpu = mInitialCpuUsage.delta(mOtaCpuUsage);
        CpuUsage patchCpu = mInitialCpuUsage.delta(mPatchCpuUsage);
        CpuUsage d2oCpu = mPatchCpuUsage.delta(mOtaCpuUsage);

        Map<String, String> results = new HashMap<>();
        // Time taken for Update Engine execution
        results.put(TIME_OVERALL_METRICNAME, Double.toString(totalTime / SECONDS_TO_NANO));
        // Time taken for Update Engine patching phase
        results.put(TIME_PATCH_METRICNAME, Double.toString(patchTime / SECONDS_TO_NANO));
        // Time taken for Update Engine finalizing phase
        results.put(TIME_FINALIZE_METRICNAME, Double.toString(d2oTime / SECONDS_TO_NANO));
        listener.testStarted(TIME_TESTID);
        listener.testEnded(TIME_TESTID, results);

        results = new HashMap<>();
        // CPU usage as measured by percentage of busy clock cycles (over all cores)
        results.put(
                CPU_OVERALL_METRICNAME, Double.toString(totalCpu.load * 100.0D / totalCpu.total));
        results.put(CPU_PATCH_METRICNAME, Double.toString(patchCpu.load * 100.0D / patchCpu.total));
        results.put(CPU_FINALIZE_METRICNAME, Double.toString(d2oCpu.load * 100.0D / d2oCpu.total));
        listener.testStarted(CPU_TESTID);
        listener.testEnded(CPU_TESTID, results);

        results = new HashMap<>();
        // Max memory increase over idle during OTA
        results.put(
                MEMORY_DELTA_METRICNAME,
                Double.toString((mMaxMemoryUsage - mInitialMemoryUsage) / MB_TO_BYTE /* MiB */));
        // Max total memory usage by the device
        results.put(MEMORY_TOTAL_METRICNAME, Double.toString(mMaxMemoryUsage / MB_TO_BYTE));
        listener.testStarted(MEMORY_TESTID);
        listener.testEnded(MEMORY_TESTID, results);

        // If we can't find temperature, don't report it
        if (mTemperatureFile != null && mMaxTemperature > 0) {
            results = new HashMap<>();
            results.put(MAX_TEMPERATURE_METRICNAME, Double.toString(mMaxTemperature / 10.0D));
            listener.testStarted(TEMPERATURE_TESTID);
            listener.testEnded(TEMPERATURE_TESTID, results);
        }
    }

    @VisibleForTesting
    void collectMetrics() {
        if (mState != UpdaterEventType.UPDATE_START && mState != UpdaterEventType.PATCH_COMPLETE) {
            return;
        }
        long curMemUsage = getMemoryUsage();
        if (curMemUsage > mMaxMemoryUsage) {
            mMaxMemoryUsage = curMemUsage;
        }
        if (mTemperatureFile != null) {
            long curTemp = getTemp();
            if (curTemp > mMaxTemperature) {
                mMaxTemperature = curTemp;
            }
        }
    }

    @VisibleForTesting
    long getTimeNano() {
        return System.nanoTime();
    }

    private long getMemoryUsage() {
        try {
            String data = mDevice.executeShellCommand("free").split("\n")[2].split(" +")[2];
            return Long.parseLong(data);
        } catch (DeviceNotAvailableException e) {
            throw new RuntimeException(e);
        }
    }

    private long getTemp() {
        try {
            String data = mDevice.executeShellCommand("cat " + mTemperatureFile);
            return Long.parseLong(data.trim());
        } catch (DeviceNotAvailableException e) {
            throw new RuntimeException(e);
        }
    }

    private CpuUsage getCpuUsage() {
        String[] data;
        try {
            data = mDevice.executeShellCommand("cat /proc/stat").split("\n")[0].split(" +");
        } catch (DeviceNotAvailableException e) {
            throw new RuntimeException(e);
        }
        long user = Long.parseLong(data[1]);
        long nice = Long.parseLong(data[2]);
        long system = Long.parseLong(data[3]);
        long idle = Long.parseLong(data[4]);
        long iowait = Long.parseLong(data[5]);
        long irq = Long.parseLong(data[6]);
        long softirq = Long.parseLong(data[7]);

        long load = user + nice + system + irq + softirq;
        long total = load + idle + iowait;

        return new CpuUsage(load, total);
    }
}
