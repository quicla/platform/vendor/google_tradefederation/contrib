// Copyright 2016 Google Inc.  All Rights Reserved.

package com.google.android.tradefed.ota;

import com.android.loganalysis.util.ArrayUtil;
import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.targetprep.BaseTargetPreparer;
import com.android.tradefed.targetprep.BuildError;
import com.android.tradefed.targetprep.ITargetCleaner;
import com.android.tradefed.targetprep.TargetSetupError;
import com.android.tradefed.util.RunUtil;

import java.util.ArrayList;
import java.util.List;

/** A preparer that set up Gservices flags before OTA testing. */
public class OtaGservicesOverridePreparer extends BaseTargetPreparer implements ITargetCleaner {

    private static final String OVERRIDE_CMD_HEAD =
            "am broadcast -a com.google.gservices.intent.action.GSERVICES_OVERRIDE ";
    private static final String FAKE_SUCCESS_MESSAGE = "Fake update succeeded.";
    private static final String FAKE_FAILURE_MESSAGE = "Fake update failed.";
    private static final String FAKE_MOBILE_NET_DELAY = "15";
    private static final String FAKE_PREFER_DATA = "1";
    // The "small" package used by the fake-ota script. It is intended only for userdebug builds
    // and will fail to verify on release builds.
    private static final String FAKE_DEFAULT_PKG =
            "http://android.clients.google.com/packages/internal/"
                    + "dummy-ota-ab-small.dev.ab39d95e.zip";

    @Option(
        name = "update-url",
        description = "the value of Gservices update_url. Maybe begin with \"file://\""
    )
    private String mUpdateUrl = null;

    @Option(name = "update-priority", description = "the priority of the update")
    private FakeOtaPriority mUpdatePriority = FakeOtaPriority.RECOMMENDED;

    @Option(
        name = "broadcast-headroom",
        description = "time to wait for broadcast side effects to propagate",
        isTimeVal = true
    )
    private long mHeadroom = 30000;

    @Option(name = "update-size", description = "the fake update size text")
    private String mUpdateSize = "small";

    @Option(name = "update-title", description = "the fake update title")
    private String mUpdateTitle = "fake-ota";

    @Option(name = "update-required-setup", description = "whether the update is SuW based")
    private String mUpdateRequiredSetup = "";

    @Option(name = "update-description", description = "the fake update description")
    private String mUpdateDescription = "This is a fake %s OTA update! Flop blop.";

    @Option(name = "override-attempts", description = "number of override attempts")
    private int mOverrideAttempts = 5;

    @Option(
        name = "attempt-interval",
        description = "time interval in ms between override attempts",
        isTimeVal = true
    )
    private long mAttemptIntervalMs = 10000;

    public enum FakeOtaPriority {
        RECOMMENDED("recommended", "2"),
        MANDATORY("mandatory", "3"),
        AUTOMATIC("automatic", "4");

        private String mPriority;
        private String mNumericUrgency;

        FakeOtaPriority(String priority, String numericUrgency) {
            mPriority = priority;
            mNumericUrgency = numericUrgency;
        }

        @Override
        public String toString() {
            return mPriority;
        }

        public String getNumericUrgency() {
            return mNumericUrgency;
        }
    }

    @Override
    public void setUp(ITestDevice device, IBuildInfo buildInfo)
            throws TargetSetupError, BuildError, DeviceNotAvailableException {
        override(device, getSetupArgs());
        RunUtil.getDefault().sleep(mHeadroom);
    }

    @Override
    public void tearDown(ITestDevice device, IBuildInfo buildInfo, Throwable e)
            throws DeviceNotAvailableException {
        override(device, getResetArgs());
    }

    private List<String> getSetupArgs() {
        List<String> args = new ArrayList<>();
        String updateUrl = mUpdateUrl != null ? mUpdateUrl : FAKE_DEFAULT_PKG;
        args.add(String.format("-e update_url %s", updateUrl));
        String updateDescription = String.format(mUpdateDescription, mUpdatePriority.toString());
        args.add(String.format("-e update_description \"%s\"", updateDescription));
        args.add(String.format("-e update_urgency \"%s\"", mUpdatePriority.getNumericUrgency()));
        args.add(String.format("-e update_required_setup \"%s\"", mUpdateRequiredSetup));
        args.add(String.format("-e update_size \"%s\"", mUpdateSize));
        args.add(String.format("-e update_title \"%s\"", mUpdateTitle));
        args.add(String.format("-e update_install_success_message \"%s\"", FAKE_SUCCESS_MESSAGE));
        args.add(String.format("-e update_install_failure_message \"%s\"", FAKE_FAILURE_MESSAGE));
        args.add(String.format("-e update_mobile_network_delay \"%s\"", FAKE_MOBILE_NET_DELAY));
        args.add(String.format("-e update_download_prefer_data \"%s\"", FAKE_PREFER_DATA));
        return args;
    }

    private List<String> getResetArgs() {
        List<String> args = new ArrayList<>();
        args.add("--esn update_url");
        args.add("--esn update_description");
        args.add("--esn update_urgency");
        args.add("--esn update_required_setup");
        args.add("--esn update_size");
        args.add("--esn update_title");
        args.add("--esn update_install_success_message");
        args.add("--esn update_install_failure_message");
        args.add("--esn update_download_prefer_data");
        args.add("--esn update_mobile_network_delay");
        return args;
    }

    private void override(ITestDevice device, List<String> args)
            throws DeviceNotAvailableException {
        String cmd = OVERRIDE_CMD_HEAD + ArrayUtil.join(" ", args);
        CLog.i("Gservices override command: %s", cmd);

        // tolerate the first N-1 failures
        int attempts = mOverrideAttempts - 1;
        while (attempts > 0) {
            attempts -= 1;
            try {
                device.executeShellCommand(cmd);
                return;
            } catch (DeviceNotAvailableException e) {
                CLog.w("Attempt to override Gservices flag, but device is unresponsive.");
                RunUtil.getDefault().sleep(mAttemptIntervalMs);
            }
        }
        // if we have not yet succeeded, try one last time and let the exception propagate
        device.executeShellCommand(cmd);
    }
}
