package com.google.android.tradefed.ota.util;

import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;

/** Utility for common bootctl tasks. */
public class BootctlUtil {

    /** Returns the current boot slot ("0" or "1"). */
    public static String getCurrentSlot(ITestDevice device) throws DeviceNotAvailableException {
        String slot = firstLine(device.executeShellCommand("bootctl get-current-slot"));
        CLog.d("\"bootctl get-current-slot\" returned %s", slot);
        return slot;
    }

    /** Checks if a slot was marked successful. */
    public static boolean isSlotMarkedSuccessful(ITestDevice device, String slot)
            throws DeviceNotAvailableException {
        // be sure to check the exit code from the same shell
        String cmd =
                String.format(
                        "bootctl is-slot-marked-successful %s >/dev/null 2>&1 || echo $?", slot);
        String exitCode = firstLine(device.executeShellCommand(cmd));
        if (exitCode.isEmpty()) {
            // echo didn't run if the first command succeeded
            exitCode = "0";
        }
        CLog.d(
                "\"bootctl is-slot-marked-successful %s\" exited with return code %s",
                slot, exitCode);
        return exitCode.equals("0");
    }

    /** Checks if two boot slots were opposite. */
    public static boolean isSlotSwapped(String before, String after) {
        return ("0".equals(before) && "1".equals(after))
                || ("1".equals(before) && "0".equals(after));
    }

    /** Helper function to remove the extra (empty) lines from shell command output. */
    private static String firstLine(String s) {
        return s.split(System.lineSeparator())[0];
    }
}
