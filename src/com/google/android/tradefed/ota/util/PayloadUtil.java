// Copyright 2017 Google Inc. All Rights Reserved.
package com.google.android.tradefed.ota.util;

import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.util.ArrayUtil;
import com.android.tradefed.util.ZipUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.google.common.annotations.VisibleForTesting;

/** Utilities for payload. */
public class PayloadUtil {

    /** Representation of an update payload. */
    public static class Payload {
        private static final String PAYLOAD_BINARY = "payload.bin";
        private static final String PAYLOAD_PROPERTIES = "payload_properties.txt";

        // Standard zip entry header length, used to calculate payload offset in OTA zip
        private static final long BASE_HEADER_LENGTH = 30L;

        private long mPayloadOffset;
        private long mPayloadLength;
        private List<String> mPayloadProperties = new ArrayList<>();

        //TODO(ghliu): investigate if we can/should expose ZipEntry.getDataOffset()
        /**
         * Creates a {@link Payload} object and inits the payload info (offset, length, properties)
         * with the given OTA zip file.
         */
        public Payload(String otaZipFileName) {
            boolean found = false;
            ZipFile otaZip = null;
            try {
                otaZip = new ZipFile(otaZipFileName);

                // iterate the zip entries to find the payload offset
                long currentOffset = 0; // offset to *data* of the current entry
                Enumeration<? extends ZipEntry> entries = otaZip.entries();
                while (entries.hasMoreElements()) {
                    ZipEntry entry = entries.nextElement();
                    String entryName = entry.getName();

                    // calculate the offset
                    // https://en.wikipedia.org/wiki/Zip_(file_format)#File_headers
                    currentOffset += BASE_HEADER_LENGTH; // standard header info
                    currentOffset += entryName.length(); // length of the file name
                    long extraLength = entry.getExtra() == null ? 0 : entry.getExtra().length;
                    currentOffset += extraLength; // extra OS-specific attributes

                    // skip directories
                    if (entry.isDirectory()) {
                        continue;
                    }
                    long entrySize = entry.getCompressedSize();

                    // check for entries of our interest
                    if (PAYLOAD_BINARY.equals(entryName)) {
                        CLog.i(
                                "Found payload binary entry at offset "
                                        + currentOffset
                                        + " size of "
                                        + entrySize);
                        if (entry.getMethod() != ZipEntry.STORED) {
                            throw new RuntimeException("Invalid compression method");
                        }
                        found = true;
                        mPayloadOffset = currentOffset;
                        mPayloadLength = entrySize;
                    } else if (PAYLOAD_PROPERTIES.equals(entryName)) {
                        CLog.i(
                                "Found payload properties entry at offset "
                                        + currentOffset
                                        + " size of "
                                        + entrySize);
                        BufferedReader br =
                                new BufferedReader(
                                        new InputStreamReader(otaZip.getInputStream(entry)));
                        String line;
                        while ((line = br.readLine()) != null) {
                            mPayloadProperties.add(line);
                        }
                        br.close();
                    }

                    // calculate the offset
                    currentOffset += entrySize; // size of data
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                ZipUtil.closeZip(otaZip);
            }
            if (!found) {
                throw new RuntimeException("Failed to find payload.bin in payload zip");
            }
        }

        @VisibleForTesting
        /** Creates a {@link Payload} object with the given payload info. */
        public Payload(long payloadOffset, long payloadLength, List<String> payloadProperties) {
            mPayloadOffset = payloadOffset;
            mPayloadLength = payloadLength;
            mPayloadProperties = new ArrayList<>(payloadProperties);
        }

        public long getPayloadOffset() {
            return mPayloadOffset;
        }

        public long getPayloadLength() {
            return mPayloadLength;
        }

        public List<String> getPayloadProperties() {
            return Collections.unmodifiableList(mPayloadProperties);
        }
    }

    /** Returns the command to start update engine with a package on the device. */
    public static String getAndroidUpdateCommand(Payload payload, String payloadUrl) {
        String payloadPropertiesString = ArrayUtil.join("\n", payload.getPayloadProperties());
        return String.format(
                "update_engine_client --update --follow --payload=%s --offset=%d --size=%d --headers=\"%s\"",
                payloadUrl,
                payload.getPayloadOffset(),
                payload.getPayloadLength(),
                payloadPropertiesString);
    }

    /** Returns the command to start update engine with Omaha (streaming). */
    public static String getOmahaUpdateCommand(String omahaUrl) {
        return String.format("update_engine_client --update --follow --omaha_url=%s", omahaUrl);
    }
}
