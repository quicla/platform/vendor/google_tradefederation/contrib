// Copyright 2017 Google Inc. All Rights Reserved.

package com.google.android.tradefed.ota;

import com.android.tradefed.build.VersionedFile;
import com.android.tradefed.config.Option;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.result.ITestInvocationListener;
import com.android.tradefed.testtype.PythonUnitTestRunner;
import com.android.tradefed.util.FileUtil;
import com.android.tradefed.util.ZipUtil2;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.compress.archivers.zip.ZipFile;

/** A {@link PythonUnitTestRunner} for OTA releasetools. */
public class ReleaseToolsUnitTest extends PythonUnitTestRunner {

    // TODO(b/70538639): randomize parent dir name so parallel tests don't collide
    @Option(name = "otatools-unzip-dir", description = "directory to unzip otatools.zip to")
    private File mOtatoolsUnzipDir = null;

    @Option(name = "dirs-in-path", description = "list of dirs to add to PATH")
    private Set<String> mDirsInPath = new HashSet<>();

    @Option(
        name = "otatools-child-dirs-in-path",
        description =
                "list of otatools child dirs to add to PATH, relative to the unzipped otatools dir"
    )
    private Set<String> mOtatoolsChildDirsInPath = new HashSet<>();

    /** Extracts otatools.zip and runs releasetools unit tests. */
    @Override
    public void run(ITestInvocationListener listener) throws DeviceNotAvailableException {
        File otatools = findOtatools();
        try {
            ZipUtil2.extractZip(new ZipFile(otatools), mOtatoolsUnzipDir);
            String path = String.join(":", getPathDirs());
            CLog.i("Set PATH=%s", path);
            getRunUtil().setEnvVariable("PATH", path);
            super.run(listener);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            FileUtil.recursiveDelete(mOtatoolsUnzipDir);
        }
    }

    /**
     * Finds otatools.zip from downloaded build artifacts. Throws an exception if file not exist.
     */
    private File findOtatools() {
        for (VersionedFile f : getBuild().getFiles()) {
            if (f.getFile().getName().matches("otatools.*")) {
                return f.getFile();
            }
        }
        throw new RuntimeException("Cannot find otatools.zip.");
    }

    /** Returns absolute paths to all directories that need to be in PATH. */
    private Set<String> getPathDirs() {
        Set<String> pathDirs = new HashSet<>();
        // add absolute paths
        pathDirs.addAll(mDirsInPath);
        // add relative paths
        for (String child : mOtatoolsChildDirsInPath) {
            pathDirs.add(new File(mOtatoolsUnzipDir, child).getAbsolutePath());
        }
        return pathDirs;
    }
}
