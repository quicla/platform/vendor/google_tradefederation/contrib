// Copyright 2017 Google Inc. All Rights Reserved.
package com.google.android.tradefed;

import com.google.android.tradefed.adb.AdbPerformanceTestTest;
import com.google.android.tradefed.adb.AdbTestClusterCommandSchedulerTest;
import com.google.android.tradefed.clockwork.WeedTestRunnerTest;
import com.google.android.tradefed.ota.IncrementalOtaLaunchControlProviderTest;
import com.google.android.tradefed.ota.OtaDeviceLaunchControlProviderTest;
import com.google.android.tradefed.ota.OtaFunctionalTestTest;
import com.google.android.tradefed.ota.OtaPerformanceTestTest;
import com.google.android.tradefed.ota.OtaconfigDeviceLaunchControlProviderTest;
import com.google.android.tradefed.ota.PostOtaVersionCheckingTestTest;
import com.google.android.tradefed.ota.VersionCachePreparerTest;
import com.google.android.tradefed.ota.util.BootctlUtilTest;
import com.google.android.tradefed.ota.util.PayloadUtilTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * A test suite for all Google Trade Federation Contrib Project unit tests.
 *
 * <p>All tests listed here should be self-contained, and do not require any external dependencies.
 */
@RunWith(Suite.class)
@SuiteClasses({
    // NOTE: please keep classes sorted lexicographically in each group
    // adb
    AdbPerformanceTestTest.class,
    AdbTestClusterCommandSchedulerTest.class,

    // clockwork
    WeedTestRunnerTest.class,

    // ota
    IncrementalOtaLaunchControlProviderTest.class,
    OtaDeviceLaunchControlProviderTest.class,
    OtaFunctionalTestTest.class,
    OtaconfigDeviceLaunchControlProviderTest.class,
    PostOtaVersionCheckingTestTest.class,
    VersionCachePreparerTest.class,
    OtaPerformanceTestTest.class,
    // TODO(ghliu): fix failed tests
    // OtaFullDowngradePreparerTest.class,

    //ota.util
    BootctlUtilTest.class,
    PayloadUtilTest.class,
})
public class ContribUnitTests {
    // empty on purpose
}
