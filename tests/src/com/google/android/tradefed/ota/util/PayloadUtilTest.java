// Copyright 2017 Google Inc. All Rights Reserved.
package com.google.android.tradefed.ota.util;

import static com.google.android.tradefed.ota.util.PayloadUtil.Payload;
import static com.google.android.tradefed.ota.util.PayloadUtil.getAndroidUpdateCommand;
import static com.google.android.tradefed.ota.util.PayloadUtil.getOmahaUpdateCommand;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/** Unit tests for {@link PayloadUtil}. */
@RunWith(JUnit4.class)
public class PayloadUtilTest {

    @Test
    public void testGetAndroidUpdateCommand() throws Exception {
        List<String> properties = new ArrayList<>(Arrays.asList("FOO=foo", "BAR=bar"));
        Payload payload = new Payload(100, 200, properties);
        String payloadUrl = "file://data/ota_pacakge/update.zip ";
        String expectedCmd =
                String.format(
                        "update_engine_client --update --follow --payload=%s --offset=100 --size=200 --headers=\"FOO=foo\nBAR=bar\"",
                        payloadUrl);
        String actualCmd = getAndroidUpdateCommand(payload, payloadUrl);
        Assert.assertEquals(expectedCmd, actualCmd);
    }

    @Test
    public void testGetOmahaUpdateCommand() throws Exception {
        String omahaUrl = "'http://127.0.0.1:1234/payload'";
        String expectedCmd =
                String.format("update_engine_client --update --follow --omaha_url=%s", omahaUrl);
        String actualCmd = getOmahaUpdateCommand(omahaUrl);
        Assert.assertEquals(expectedCmd, actualCmd);
    }
}
