package com.google.android.tradefed.ota.util;

import com.android.tradefed.device.ITestDevice;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/** Unit tests for {@link BootctlUtil}. */
@RunWith(JUnit4.class)
public class BootctlUtilTest {
    ITestDevice mMockDevice;

    @Before
    public void setUp() throws Exception {
        mMockDevice = EasyMock.createMock(ITestDevice.class);
    }

    @Test
    public void testGetCurrentSlot() throws Exception {
        EasyMock.expect(mMockDevice.executeShellCommand(EasyMock.anyObject()))
                .andReturn("0" + System.lineSeparator());
        EasyMock.replay(mMockDevice);
        Assert.assertEquals("0", BootctlUtil.getCurrentSlot(mMockDevice));
        EasyMock.verify(mMockDevice);
    }

    @Test
    public void testSlotMarkedSuccessful() throws Exception {
        EasyMock.expect(mMockDevice.executeShellCommand(EasyMock.anyObject())).andReturn("");
        EasyMock.replay(mMockDevice);
        Assert.assertTrue(BootctlUtil.isSlotMarkedSuccessful(mMockDevice, "foo"));
        EasyMock.verify(mMockDevice);
    }

    @Test
    public void testSlotMarkedUnsuccessful() throws Exception {
        EasyMock.expect(mMockDevice.executeShellCommand(EasyMock.anyObject()))
                .andReturn("70" + System.lineSeparator());
        EasyMock.replay(mMockDevice);
        Assert.assertFalse(BootctlUtil.isSlotMarkedSuccessful(mMockDevice, "foo"));
        EasyMock.verify(mMockDevice);
    }

    @Test
    public void testCheckSlotSwapped() throws Exception {
        Assert.assertTrue(BootctlUtil.isSlotSwapped("0", "1"));
        Assert.assertTrue(BootctlUtil.isSlotSwapped("1", "0"));
        Assert.assertFalse(BootctlUtil.isSlotSwapped("0", "0"));
        Assert.assertFalse(BootctlUtil.isSlotSwapped("1", "1"));
        // invalid slot names
        Assert.assertFalse(BootctlUtil.isSlotSwapped("foo", "bar"));
    }
}
