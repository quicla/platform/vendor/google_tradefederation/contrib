package com.google.android.tradefed.ota;

import com.android.tradefed.build.BuildInfo;
import com.android.tradefed.result.ITestInvocationListener;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class PostOtaVersionCheckingTestTest {
    ITestInvocationListener mMockListener;
    PostOtaVersionCheckingTest mTest;

    @Before
    public void setUp() throws Exception {
        mMockListener = EasyMock.createMock(ITestInvocationListener.class);
        mTest = new PostOtaVersionCheckingTest(mMockListener);
        mTest.setBuild(new BuildInfo());
    }

    @Test
    public void testCanSkipMatch() {
        expectPass();
        mTest.checkVersionAndReport("", "foo", "foo", true);
        EasyMock.verify(mMockListener);
    }

    @Test
    public void testCanSkipNotMatch() {
        expectFail();
        mTest.checkVersionAndReport("", "foo", "bar", true);
        EasyMock.verify(mMockListener);
    }

    @Test
    public void testCanSkipExpectedNull() {
        expectSkip();
        mTest.checkVersionAndReport("", null, "bar", true);
        EasyMock.verify(mMockListener);
    }

    @Test
    public void testCanSkipActualNull() {
        expectSkip();
        mTest.checkVersionAndReport("", "foo", null, true);
        EasyMock.verify(mMockListener);
    }

    @Test
    public void testCanSkipBothNull() {
        expectSkip();
        mTest.checkVersionAndReport("", null, null, true);
        EasyMock.verify(mMockListener);
    }

    @Test
    public void testNoSkipMatch() {
        expectPass();
        mTest.checkVersionAndReport("", "foo", "foo", false);
        EasyMock.verify(mMockListener);
    }

    @Test
    public void testNoSkipNotMatch() {
        expectFail();
        mTest.checkVersionAndReport("", "foo", "bar", false);
        EasyMock.verify(mMockListener);
    }

    @Test
    public void testNoSkipExpectedNull() {
        // always skip when the expected value is null
        expectSkip();
        mTest.checkVersionAndReport("", null, "bar", false);
        EasyMock.verify(mMockListener);
    }

    @Test
    public void testNoSkipActualNull() {
        expectFail();
        mTest.checkVersionAndReport("", "foo", null, false);
        EasyMock.verify(mMockListener);
    }

    @Test
    public void testNoSkipBothNull() {
        // always skip when the expected value is null
        expectSkip();
        mTest.checkVersionAndReport("", null, null, false);
        EasyMock.verify(mMockListener);
    }

    private void expectPass() {
        mMockListener.testStarted(EasyMock.anyObject());
        EasyMock.expectLastCall().once();
        mMockListener.testEnded(EasyMock.anyObject(), EasyMock.anyObject());
        EasyMock.expectLastCall().once();
        EasyMock.replay(mMockListener);
    }

    private void expectFail() {
        mMockListener.testStarted(EasyMock.anyObject());
        EasyMock.expectLastCall().once();
        mMockListener.testFailed(EasyMock.anyObject(), EasyMock.anyObject());
        EasyMock.expectLastCall().once();
        mMockListener.testEnded(EasyMock.anyObject(), EasyMock.anyObject());
        EasyMock.expectLastCall().once();
        EasyMock.replay(mMockListener);
    }

    private void expectSkip() {
        mMockListener.testStarted(EasyMock.anyObject());
        EasyMock.expectLastCall().once();
        mMockListener.testIgnored(EasyMock.anyObject());
        EasyMock.expectLastCall().once();
        mMockListener.testEnded(EasyMock.anyObject(), EasyMock.anyObject());
        EasyMock.expectLastCall().once();
        EasyMock.replay(mMockListener);
    }
}
