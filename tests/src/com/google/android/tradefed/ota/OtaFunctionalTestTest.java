// Copyright 2017 Google Inc.  All Rights Reserved.
package com.google.android.tradefed.ota;

import static com.google.android.tradefed.ota.OtaFunctionalTest.OtaType.FULL;
import static com.google.android.tradefed.ota.OtaFunctionalTest.OtaType.INCREMENTAL;
import static com.google.android.tradefed.ota.util.PayloadUtil.Payload;

import static org.easymock.EasyMock.anyLong;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.createStrictMock;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import com.android.ddmlib.IDevice;
import com.android.ddmlib.IShellOutputReceiver;
import com.android.ddmlib.testrunner.TestIdentifier;
import com.android.tradefed.build.DeviceBuildInfo;
import com.android.tradefed.build.IDeviceBuildInfo;
import com.android.tradefed.build.OtaDeviceBuildInfo;
import com.android.tradefed.config.OptionSetter;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ILogcatReceiver;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.result.ITestInvocationListener;
import com.android.tradefed.result.InputStreamSource;
import com.android.tradefed.util.UpdaterEventType;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/** Unit tests for {@link OtaFunctionalTest}. */
@RunWith(JUnit4.class)
public class OtaFunctionalTestTest {

    /** The {@link OtaFunctionalTest} under test, with all dependencies mocked out */
    private OtaFunctionalTest mOtaFunctionalTest;

    private ITestDevice mMockTestDevice;
    private ITestInvocationListener mMockListener;
    private PipedOutputStream mMockPipe = null;
    private ILogcatReceiver mMockReceiver;

    private static final String[] LOG_SUCCESS =
            new String[] {
                "01-01 00:03:16.767   734   734 I update_engine: FILLER\n",
                "01-01 00:04:32.653   747   747 I update_engine: "
                        + "[0101/000232:INFO:update_attempter_android.cc(240)] "
                        + "Using this install plan:\n",
                "01-01 00:05:10.640   747   747 I update_engine: "
                        + "[0101/000510:INFO:action_processor.cc(116)] ActionProcessor: finished "
                        + "FilesystemVerifierAction with code ErrorCode::kSuccess\n",
                "01-01 00:14:02.327   734   734 I update_engine: "
                        + "[0101/001402:INFO:update_attempter_android.cc(304)] "
                        + "Update successfully applied, waiting to reboot.\n",
                "01-01 00:03:16.767   734   734 I update_engine: FILLER\n"
            };
    private static final String[] LOG_FAILURE =
            new String[] {
                "01-01 00:03:16.767   734   734 I update_engine: FILLER\n",
                "01-01 00:04:32.653   747   747 I update_engine: "
                        + "[0101/000232:INFO:update_attempter_android.cc(240)] "
                        + "Using this install plan:\n",
                "01-01 00:14:02.327   734   734 I update_engine: "
                        + "[0101/000644:INFO:action_processor.cc(116)] "
                        + "ActionProcessor: finished FilesystemVerifierAction with code "
                        + "ErrorCode::kNewRootfsVerificationError\n",
                "01-01 00:03:16.767   734   734 I update_engine: FILLER\n"
            };

    private List<UpdaterEventType> mCalledEvents = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        mMockReceiver = createNiceMock(ILogcatReceiver.class);
        mMockPipe = new PipedOutputStream();
        final PipedInputStream p = new PipedInputStream();
        try {
            mMockPipe.connect(p);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        EasyMock.expect(mMockReceiver.getLogcatData())
                .andReturn(
                        new InputStreamSource() {
                            @Override
                            public InputStream createInputStream() {
                                return p;
                            }

                            @Override
                            public void close() {
                                // ignore
                            }

                            @Override
                            public long size() {
                                return 0;
                            }
                        });
        EasyMock.replay(mMockReceiver);

        mOtaFunctionalTest =
                new OtaFunctionalTest(mMockReceiver) {
                    @Override
                    void kickoffSideloadUpdate() throws DeviceNotAvailableException {
                        // do nothing
                    }

                    @Override
                    protected void onEvent(UpdaterEventType event) {
                        mCalledEvents.add(event);
                    }
                };
        OptionSetter setter = new OptionSetter(mOtaFunctionalTest);
        setter.setOptionValue("echo-uec-output", "false");

        OtaDeviceBuildInfo build = new OtaDeviceBuildInfo();
        build.setBaselineBuild(new DeviceBuildInfo());
        build.setOtaBuild(new DeviceBuildInfo());
        build.setTestTag("foo");
        mOtaFunctionalTest.setBuild(build);

        mMockTestDevice = createMock(ITestDevice.class);
        mOtaFunctionalTest.setDevice(mMockTestDevice);

        mMockListener = createStrictMock(ITestInvocationListener.class);
    }

    private void feedMockPipe(String[] logLines) {
        for (String line : logLines) {
            try {
                mMockPipe.write(line.getBytes());
            } catch (IOException e) {
                fail(e.getLocalizedMessage());
            }
        }
    }

    private void expectCallsToListener(int testCount, boolean[] pass) {
        mMockListener.testRunStarted(isA(String.class), eq(testCount));
        expectLastCall();
        for (int i = 0; i < testCount; i++) {
            mMockListener.testStarted(isA(TestIdentifier.class));
            expectLastCall();
            if (!pass[i]) {
                mMockListener.testFailed(isA(TestIdentifier.class), isA(String.class));
                expectLastCall();
            }
            mMockListener.testEnded(isA(TestIdentifier.class), isA(Map.class));
            expectLastCall();
        }
        mMockListener.testRunEnded(anyLong(), isA(Map.class));
        expectLastCall();
        replay(mMockListener);
    }

    private void expectCallsToDevice(String slotBefore, String slotAfter, boolean skipReboot)
            throws Exception {
        mMockTestDevice.executeShellCommand(isA(String.class));
        EasyMock.expectLastCall().andReturn(slotBefore);
        if (skipReboot) {
            replay(mMockTestDevice);
            return;
        }
        mMockTestDevice.reboot();
        expectLastCall();
        mMockTestDevice.executeShellCommand(isA(String.class));
        EasyMock.expectLastCall().andReturn(slotAfter);
        replay(mMockTestDevice);
    }

    @Test(timeout = 1000L)
    public void testUpdateSucceeded() throws Exception {
        boolean[] pass = {true, true};
        expectCallsToListener(2, pass);
        expectCallsToDevice("0", "1", false);

        feedMockPipe(LOG_SUCCESS);
        mOtaFunctionalTest.run(mMockListener);
        verify(mMockListener, mMockTestDevice);
        assertEquals(3, mCalledEvents.size());
        assertEquals(UpdaterEventType.UPDATE_START, mCalledEvents.get(0));
        assertEquals(UpdaterEventType.PATCH_COMPLETE, mCalledEvents.get(1));
        assertEquals(UpdaterEventType.UPDATE_COMPLETE, mCalledEvents.get(2));
    }

    @Test(timeout = 1000L)
    public void testUpdateSucceededCheckSlotFailed() throws Exception {
        boolean[] pass = {true, false};
        expectCallsToListener(2, pass);
        expectCallsToDevice("0", "0", false);

        feedMockPipe(LOG_SUCCESS);
        mOtaFunctionalTest.run(mMockListener);
        verify(mMockListener, mMockTestDevice);
        assertEquals(3, mCalledEvents.size());
        assertEquals(UpdaterEventType.UPDATE_START, mCalledEvents.get(0));
        assertEquals(UpdaterEventType.PATCH_COMPLETE, mCalledEvents.get(1));
        assertEquals(UpdaterEventType.UPDATE_COMPLETE, mCalledEvents.get(2));
    }

    @Test(timeout = 1000L)
    public void testUpdateSucceededSkipReboot() throws Exception {
        boolean[] pass = {true};
        expectCallsToListener(1, pass);
        expectCallsToDevice("0", "1", true);
        OptionSetter setter = new OptionSetter(mOtaFunctionalTest);
        setter.setOptionValue("skip-post-ota-reboot", "true");

        feedMockPipe(LOG_SUCCESS);
        mOtaFunctionalTest.run(mMockListener);
        verify(mMockListener, mMockTestDevice);
        assertEquals(3, mCalledEvents.size());
        assertEquals(UpdaterEventType.UPDATE_START, mCalledEvents.get(0));
        assertEquals(UpdaterEventType.PATCH_COMPLETE, mCalledEvents.get(1));
        assertEquals(UpdaterEventType.UPDATE_COMPLETE, mCalledEvents.get(2));
    }

    @Test(timeout = 1000L)
    public void testUpdateFailed() throws Exception {
        mMockListener.testRunStarted(isA(String.class), eq(2));
        expectLastCall().once();
        replay(mMockListener);
        expectCallsToDevice("0", "0", false);

        feedMockPipe(LOG_FAILURE);
        try {
            mOtaFunctionalTest.run(mMockListener);
            fail("expecting failure");
        } catch (Exception e) {
            // expected
        }
        verify(mMockListener);
        assertEquals(2, mCalledEvents.size());
        assertEquals(UpdaterEventType.UPDATE_START, mCalledEvents.get(0));
        assertEquals(UpdaterEventType.ERROR, mCalledEvents.get(1));
    }

    @Test
    public void testGetFullOtaPackage() throws Exception {
        IDeviceBuildInfo baselineBuild = new DeviceBuildInfo();
        baselineBuild.setOtaPackageFile(new File("baseline"), "0");
        IDeviceBuildInfo otaBuild = new DeviceBuildInfo();
        otaBuild.setOtaPackageFile(new File("ota"), "0");
        OtaDeviceBuildInfo build = new OtaDeviceBuildInfo();
        build.setBaselineBuild(baselineBuild);
        build.setOtaBuild(otaBuild);
        assertEquals("ota", mOtaFunctionalTest.getOtaPackage(build, FULL).getName());
    }

    @Test
    public void testGetIncrementalOtaPackage() throws Exception {
        IDeviceBuildInfo baselineBuild = new DeviceBuildInfo();
        baselineBuild.setOtaPackageFile(new File("baseline"), "0");
        OtaDeviceBuildInfo build = new OtaDeviceBuildInfo();
        build.setBaselineBuild(baselineBuild);
        assertEquals("baseline", mOtaFunctionalTest.getOtaPackage(build, INCREMENTAL).getName());
    }

    @Test
    public void testKickoffSideloadUpdate() throws Exception {
        File fakeOtaPackage = new File("foo");
        mOtaFunctionalTest =
                new OtaFunctionalTest(mMockReceiver) {
                    @Override
                    File getOtaPackage(OtaDeviceBuildInfo build, OtaType type) {
                        return fakeOtaPackage;
                    }

                    @Override
                    Payload initPayload(final String filename) {
                        return new Payload(0, 0, new ArrayList<>());
                    }
                };
        new OptionSetter(mOtaFunctionalTest).setOptionValue("echo-uec-output", "false");
        mOtaFunctionalTest.setDevice(mMockTestDevice);
        IDevice mockIDevice = createMock(IDevice.class);
        mockIDevice.executeShellCommand(
                isA(String.class), isA(IShellOutputReceiver.class), anyLong(), isA(TimeUnit.class));
        expectLastCall().once();
        expect(mMockTestDevice.getSerialNumber()).andStubReturn("");
        expect(mMockTestDevice.getIDevice()).andStubReturn(mockIDevice);
        expect(mMockTestDevice.pushFile(isA(File.class), isA(String.class))).andStubReturn(true);
        replay(mockIDevice, mMockTestDevice);

        mOtaFunctionalTest.kickoffSideloadUpdate();
        // wait a bit since the uec command is executed on a separate thread
        Thread.sleep(1000);
        verify(mockIDevice, mMockTestDevice);
    }
}
