package com.google.android.tradefed.ota;

import static com.google.android.tradefed.ota.VersionCachePreparer.getPostOtaBuild;

import static org.junit.Assert.fail;

import com.android.tradefed.build.DeviceBuildInfo;
import com.android.tradefed.build.IDeviceBuildInfo;
import com.android.tradefed.build.OtaDeviceBuildInfo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/** Unit tests for {@link VersionCachePreparer} */
@RunWith(JUnit4.class)
public class VersionCachePreparerTest {
    private static final String GENERIC_BUILD = "Generic Build";
    private static final String OTA_BUILD_OTA = "OTA Build: OTA";
    private static final String OTA_BUILD_BASELINE = "OTA Build: Baseline";

    /** Create an {@link IDeviceBuildInfo}. */
    private static IDeviceBuildInfo setUpGenericBuild() {
        IDeviceBuildInfo build = new DeviceBuildInfo();
        build.setBuildId(GENERIC_BUILD);
        return build;
    }

    /** Create an {@link OtaDeviceBuildInfo} that includes a baseline build and an OTA build. */
    private static OtaDeviceBuildInfo setUpOtaBuild() {
        IDeviceBuildInfo baselineBuild = new DeviceBuildInfo();
        baselineBuild.setBuildId(OTA_BUILD_BASELINE);
        IDeviceBuildInfo otaBuild = new DeviceBuildInfo();
        otaBuild.setBuildId(OTA_BUILD_OTA);

        OtaDeviceBuildInfo build = new OtaDeviceBuildInfo();
        build.setBaselineBuild(baselineBuild);
        build.setOtaBuild(otaBuild);
        return build;
    }

    /**
     * Test if trying to get the secondary build from an {@link IDeviceBuildInfo} throws an
     * exception.
     */
    @Test
    public void testGenericDeviceBuild() throws Exception {
        try {
            IDeviceBuildInfo otaBuild = getPostOtaBuild(setUpGenericBuild(), false);
            fail("Expect failure: generic OTA build don't have a secondary build");
        } catch (RuntimeException e) {
            // expected failure
        }
    }

    /**
     * Test if we can get the primary build from an {@link IDeviceBuildInfo} with useFirstVersion.
     */
    @Test
    public void testFirstVersionGenericDeviceBuildInfo() throws Exception {
        IDeviceBuildInfo otaBuild = getPostOtaBuild(setUpGenericBuild(), true);
        Assert.assertEquals(GENERIC_BUILD, otaBuild.getBuildId());
    }

    /** Test if we can get the OTA build from an {@link OtaDeviceBuildInfo} */
    @Test
    public void testOtaDeviceBuildInfo() throws Exception {
        IDeviceBuildInfo otaBuild = getPostOtaBuild(setUpOtaBuild(), false);
        Assert.assertEquals(OTA_BUILD_OTA, otaBuild.getBuildId());
    }

    /**
     * Test if we can get the baseline build from an {@link OtaDeviceBuildInfo} with
     * useFirstVersion.
     */
    @Test
    public void testFirstVersionOtaDeviceBuildInfo() throws Exception {
        IDeviceBuildInfo otaBuild = getPostOtaBuild(setUpOtaBuild(), true);
        Assert.assertEquals(OTA_BUILD_BASELINE, otaBuild.getBuildId());
    }

    /**
     * Test if trying to get the OTA build from a malformed {@link OtaDeviceBuildInfo} throws an
     * exception.
     */
    @Test
    public void TestMalformedOtaDeviceBuildInfo() throws Exception {
        OtaDeviceBuildInfo build = setUpOtaBuild();
        build.setOtaBuild(null);
        try {
            IDeviceBuildInfo otaBuild = getPostOtaBuild(build, false);
            fail("Expect failure: null OTA build");
        } catch (RuntimeException e) {
            // expected failure
        }
    }
}
