package com.google.android.tradefed.ota;

import static org.easymock.EasyMock.contains;
import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;

import com.android.ddmlib.testrunner.TestIdentifier;
import com.android.tradefed.config.OptionSetter;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.result.ITestInvocationListener;
import com.android.tradefed.util.UpdaterEventType;

import org.easymock.Capture;
import org.easymock.CaptureType;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class OtaPerformanceTestTest {

    private static final String TEMPERATURE_FILE = "/sys/class/thermal/thermal_zone15/type";
    private static final String PROC_STAT_OUTPUT_FORMAT = "cpu  %d 0 0 %d 0 0 0 0 0 0";
    private static final String FREE_OUTPUT_FORMAT =
            "\t\ttotal        used        free      shared     buffers\n"
                    + "Mem:                0           0           0           0           0\n"
                    + "-/+ buffers/cache:     %d   0\n"
                    + "Swap:               0           0           0\n";

    private OtaPerformanceTest mOtaPerformanceTest;
    private ITestDevice mMockDevice;
    private ITestInvocationListener mMockListener;

    private Capture<TestIdentifier> mStartTestId = new Capture<>(CaptureType.ALL);
    private Capture<TestIdentifier> mEndTestId = new Capture<>(CaptureType.ALL);
    private Capture<Map<String, String>> mEndTestMetrics = new Capture<>(CaptureType.ALL);

    @Before
    public void setUp() throws Exception {
        mMockDevice = createNiceMock(ITestDevice.class);
        expect(mMockDevice.executeShellCommand(contains("grep"))).andStubReturn(TEMPERATURE_FILE);
        expect(mMockDevice.executeShellCommand(contains("cat /sys/class/thermal/")))
                .andStubReturn("500");
        expect(mMockDevice.executeShellCommand(eq("cat /proc/stat")))
                .andReturn(String.format(PROC_STAT_OUTPUT_FORMAT, 0, 0))
                .once();
        expect(mMockDevice.executeShellCommand(eq("cat /proc/stat")))
                .andReturn(String.format(PROC_STAT_OUTPUT_FORMAT, 25, 75))
                .once();
        expect(mMockDevice.executeShellCommand(eq("cat /proc/stat")))
                .andReturn(String.format(PROC_STAT_OUTPUT_FORMAT, 100, 100))
                .once();
        expect(mMockDevice.executeShellCommand(eq("free")))
                .andReturn(String.format(FREE_OUTPUT_FORMAT, (long) OtaPerformanceTest.MB_TO_BYTE))
                .once();
        expect(mMockDevice.executeShellCommand(eq("free")))
                .andReturn(
                        String.format(FREE_OUTPUT_FORMAT, (long) OtaPerformanceTest.MB_TO_BYTE * 3))
                .atLeastOnce();

        mMockListener = EasyMock.createMock(ITestInvocationListener.class);
        mMockListener.testStarted(EasyMock.capture(mStartTestId));
        EasyMock.expectLastCall().times(4);
        mMockListener.testEnded(EasyMock.capture(mEndTestId), EasyMock.capture(mEndTestMetrics));
        EasyMock.expectLastCall().times(4);

        replay(mMockDevice, mMockListener);

        mOtaPerformanceTest =
                new OtaPerformanceTest() {
                    volatile long time = 0;

                    @Override
                    long getTimeNano() {
                        time += SECONDS_TO_NANO * 10;
                        return time;
                    }
                };
        mOtaPerformanceTest.setDevice(mMockDevice);

        OptionSetter setter = new OptionSetter(mOtaPerformanceTest);
        // prevent the timer from running by setting the interval to be really high
        setter.setOptionValue("ota-performance-interval", "1d");
    }

    @Test
    public void testOtaPerformanceTest() throws Exception {
        mOtaPerformanceTest.preOtaTest();
        mOtaPerformanceTest.onEvent(UpdaterEventType.UPDATE_START);
        mOtaPerformanceTest.collectMetrics();
        mOtaPerformanceTest.onEvent(UpdaterEventType.PATCH_COMPLETE);
        mOtaPerformanceTest.collectMetrics();
        mOtaPerformanceTest.onEvent(UpdaterEventType.UPDATE_COMPLETE);
        mOtaPerformanceTest.postOtaTest(mMockListener);

        EasyMock.verify(mMockDevice, mMockListener);

        HashMap<String, Map<String, String>> result = new HashMap<>();
        for (int i = 0; i < mStartTestId.getValues().size(); i++) {
            TestIdentifier testId = mEndTestId.getValues().get(i);
            Assert.assertTrue(mStartTestId.getValues().get(i).equals(testId));
            result.put(testId.getTestName(), mEndTestMetrics.getValues().get(i));
        }

        Map<String, String> timeResult = result.get(OtaPerformanceTest.TIME_TESTID.getTestName());
        double timeOverall =
                Double.parseDouble(timeResult.get(OtaPerformanceTest.TIME_OVERALL_METRICNAME));
        double timePatch =
                Double.parseDouble(timeResult.get(OtaPerformanceTest.TIME_PATCH_METRICNAME));
        double timeFinalize =
                Double.parseDouble(timeResult.get(OtaPerformanceTest.TIME_FINALIZE_METRICNAME));
        // Make sure time measurement doesn't vary by more than 5 seconds
        Assert.assertEquals(20D, timeOverall, 5);
        Assert.assertEquals(10D, timePatch, 5);
        Assert.assertEquals(10D, timeFinalize, 5);

        Map<String, String> cpuResult = result.get(OtaPerformanceTest.CPU_TESTID.getTestName());
        double cpuOverall =
                Double.parseDouble(cpuResult.get(OtaPerformanceTest.CPU_OVERALL_METRICNAME));
        double cpuPatch =
                Double.parseDouble(cpuResult.get(OtaPerformanceTest.CPU_PATCH_METRICNAME));
        double cpuFinalize =
                Double.parseDouble(cpuResult.get(OtaPerformanceTest.CPU_FINALIZE_METRICNAME));
        Assert.assertEquals(50D, cpuOverall, 0);
        Assert.assertEquals(25D, cpuPatch, 0);
        Assert.assertEquals(75D, cpuFinalize, 0);

        Map<String, String> memoryResult =
                result.get(OtaPerformanceTest.MEMORY_TESTID.getTestName());
        double memDelta =
                Double.parseDouble(memoryResult.get(OtaPerformanceTest.MEMORY_DELTA_METRICNAME));
        double memTotal =
                Double.parseDouble(memoryResult.get(OtaPerformanceTest.MEMORY_TOTAL_METRICNAME));
        Assert.assertEquals(2D, memDelta, 0);
        Assert.assertEquals(3D, memTotal, 0);

        Map<String, String> temperatureResult =
                result.get(OtaPerformanceTest.TEMPERATURE_TESTID.getTestName());
        double tempMax =
                Double.parseDouble(
                        temperatureResult.get(OtaPerformanceTest.MAX_TEMPERATURE_METRICNAME));
        Assert.assertEquals(50D, tempMax, 0);
    }
}
