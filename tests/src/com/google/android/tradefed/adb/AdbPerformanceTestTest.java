// Copyright 2017 Google Inc. All Rights Reserved.
package com.google.android.tradefed.adb;

import static org.junit.Assert.assertEquals;

import com.android.ddmlib.testrunner.TestIdentifier;
import com.android.tradefed.result.ITestInvocationListener;

import com.google.android.tradefed.adb.AdbPerformanceTest.MultiRunResultHelper;

import org.easymock.Capture;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

/** Unit tests for {@link AdbPerformanceTest}. */
@RunWith(JUnit4.class)
public class AdbPerformanceTestTest {
    MultiRunResultHelper mHelper;
    ITestInvocationListener mMockListener;

    @Before
    public void setUp() throws Exception {
        mMockListener = EasyMock.createMock(ITestInvocationListener.class);
    }

    @Test
    public void testMultiRunHelper() throws Throwable {
        TestIdentifier expectedId = new TestIdentifier("foo", "bar");
        Map<String, String> expectedSummary = new HashMap<>();
        expectedSummary.put(String.format(AdbPerformanceTest.MIN_OF_RUNS, 3), "100.0");
        expectedSummary.put(String.format(AdbPerformanceTest.MEDIAN_OF_RUNS, 3), "200.0");
        expectedSummary.put(String.format(AdbPerformanceTest.MAX_OF_RUNS, 3), "1000.0");

        mMockListener.testStarted((TestIdentifier) EasyMock.anyObject());
        EasyMock.expectLastCall();
        Capture<TestIdentifier> actualId = new Capture<>();
        Capture<Map<String, String>> actualSummary = new Capture<>();
        mMockListener.testEnded(EasyMock.capture(actualId), EasyMock.capture(actualSummary));
        EasyMock.expectLastCall();
        EasyMock.replay(mMockListener);

        mHelper = new MultiRunResultHelper("foo", 3);
        mHelper.putRawResult("bar", 100);
        mHelper.putRawResult("bar", 200);
        mHelper.putRawResult("bar", 1000);
        mHelper.reportResults(mMockListener);
        assertEquals(expectedId, actualId.getValue());
        assertEquals(expectedSummary.entrySet(), actualSummary.getValue().entrySet());
        EasyMock.verify(mMockListener);
    }
}
